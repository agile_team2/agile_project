package db;

import java.io.FileInputStream; 
import java.util.Properties;
public class Config {
	private static Properties p = null;
	static {
		try {
			p = new Properties();
			//load the config properties file, the file path depends on you.
			p.load(new FileInputStream("property/mysql.properties"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	//get the string property from the properties file.
	public static String getValue(String key) {
		return p.get(key).toString();
	}
}

