package db;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection; 
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

/***
 * get the mysql database connection
 * @author michael
 *
 */
public class DBUtil {
	
	private Connection conn = null;
	private Statement stm = null;
	private PreparedStatement pstmt = null;
	private ResultSet rs = null;
	
	public DBUtil() {}
	
	public static DBUtil getDBUtilInstance() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
		DBUtil dbUtil = DBtilHolder.dbUtil;
		dbUtil.getConnection();
		return dbUtil;
	}
	
	public static class DBtilHolder{
		private static DBUtil dbUtil = new DBUtil();
	}

	public Connection getConnection() throws ClassNotFoundException,
			SQLException, InstantiationException, IllegalAccessException {
		//get configuration info of the mysql database by the static method of Config class
		String driver = Config.getValue("driver");
		String url = Config.getValue("url");
		String user = Config.getValue("user");
		String pwd = Config.getValue("password");
		try {
			//initial driver of mysql
			Class.forName(driver);
			//set connection up between program and database
			conn = DriverManager.getConnection(url, user, pwd);
			return conn;
		} catch (Exception e) {
			throw new SQLException("Error in the JDBC driver or connection fail!");
		}
	}
/***
 * release all resources
 */
	public void closeAll() {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (pstmt != null) {
			try {
				pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
/***
 * execute prepared statement sql, return the ResultSet.
 * @param preparedSql
 *        the prepared statement sql string
 * @param param
 *        parameters Array, all parameters must be String type.
 * @return after the sql has been executed, return the ResultSet of sql
 * @throws SQLException 
 */
	public ResultSet executeQuery(String preparedSql, String[] param) throws SQLException {
			pstmt = conn.prepareStatement(preparedSql);
			if (param != null) {
				for (int i = 0; i < param.length; i++) {
					pstmt.setString(i + 1, param[i]);
				}
			}
			rs = pstmt.executeQuery();
		return rs;
	}
	/***
	 * execute the static sql, return the ResultSet.
	 * @param sql
	 *        the common sql string with the parameters
	 * @return after the sql has been executed, return the ResultSet of sql
	 * @throws SQLException
	 */
	public ResultSet executeQuery1(String sql) throws SQLException{
		stm = (Statement) conn.createStatement();
		rs = stm.executeQuery(sql);
		return rs;
	}
/***
 * execute prepared statement sql, return the number of how many record have been affected.
 * @param preparedSql
 * 	      the prepared statement sql string
 * @param param
 *        parameters Array, all parameters must be String type.
 * @return after the sql string was executed, return the number of how many record have been affected.
 * @throws SQLException 
 */
	public int executeUpdate(String preparedSql, String[] param) throws SQLException {
		int num = 0;
			pstmt = conn.prepareStatement(preparedSql);
			if (param != null) {
				for (int i = 0; i < param.length; i++) {
					pstmt.setString(i + 1, param[i]);
				}
			}
			num = pstmt.executeUpdate();
		return num;
	}
	/***
	 * wtite the data into csv file from the ResultSet
	 * @param rs
	 * @param type
	 * @throws IOException 
	 * @throws SQLException 
	 */
	public static void writeToFile(ResultSet rs,String type) throws IOException, SQLException{
			File file = new File("CSVFiles/" + type + new SimpleDateFormat("yy_MM_dd_HH_MM_ss").format(new Date()) + ".csv");
			if(!file.exists()) {
				file.createNewFile();
			}
			FileWriter outputFile = new FileWriter(file);
			PrintWriter printWriter = new PrintWriter(outputFile);
			ResultSetMetaData rsmd = rs.getMetaData();
			int numColumns = rsmd.getColumnCount();

			for(int i=0;i<numColumns;i++){
				printWriter.print(rsmd.getColumnLabel(i+1)+",");
			}
			printWriter.print("\n");
			while(rs.next()){
				for(int i=0;i<numColumns;i++){
					printWriter.print(rs.getString(i+1)+",");
				}
				printWriter.print("\n");
				printWriter.flush();
			}
			printWriter.close();
		}
}


