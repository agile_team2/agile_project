package core;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import com.mysql.cj.xdevapi.UpdateStatementImpl;

import db.DBUtil;
import exceptions.IllegalOrderID;

public class DeliveryStatus {
	public int UpdateIsDelivered(DBUtil dbUtil, String orderNumber) throws Exception, IllegalOrderID {
		dbUtil.getConnection();
		validateOrderNumber(orderNumber);
		int sign = 0;
		sign = dbUtil.executeUpdate("update orders set isDelivered = 'YES' where orders.order_id = ? ", new String [] {orderNumber});
		
		return sign;
		
	}
	
	public boolean validateOrderNumber(String orderID) throws IllegalOrderID {
		if (!orderID.matches("^O[0-9]{6}$")) {
			throw new IllegalOrderID();
		}
		return true;
	}
	


	
}
