package core;

import java.awt.HeadlessException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import db.DBUtil;
import exceptions.IllegalOrderID;

public interface InvoiceDao {
	public static boolean generateInvoice(DBUtil db, Invoice invoice) throws SQLException {
		boolean success = false;
		int sign = 0;
		sign = db.executeUpdate(
				"insert into Invoices(invoice_id,invoice_date,payment_method,isOverdue,isPaidFor,order_id) values(?,?,?,?,?,?)",
				invoice.getAttributesArray1());
		if (sign == 1)
			success = true;
		return success;
	}
	
	public static ResultSet searchAllInvoices(DBUtil db) throws SQLException {
		return db.executeQuery1("select * from invoices");
	}
	
	public static ResultSet searchInvoiceByCustomerFirstName(String firstName,DBUtil db) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
		ResultSet resultSet = null;
		resultSet = db.executeQuery("Select Newsagency_DB.Customers.firstName, Newsagency_DB.Customers.lastName, Newsagency_DB.Invoices.invoice_id, Newsagency_DB.Invoices.invoice_date, Newsagency_DB.Invoices.payment_method, Newsagency_DB.Invoices.order_id From Newsagency_DB.Invoices inner join Newsagency_DB.Orders on Newsagency_DB.Invoices.order_id = Newsagency_DB.Orders.order_id inner join Newsagency_DB.Customers on Newsagency_DB.Orders.customer_id = Newsagency_DB.Customers.customer_id Where Newsagency_DB.Customers.firstName = ?;", new String [] {firstName});
		return resultSet;
	}
	
	public static ResultSet searchInvoiceByCustomerLastName(String lastName,DBUtil db) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
		ResultSet resultSet = null;
		resultSet = db.executeQuery("Select Newsagency_DB.Customers.firstName, Newsagency_DB.Customers.lastName, Newsagency_DB.Invoices.invoice_id, Newsagency_DB.Invoices.invoice_date, Newsagency_DB.Invoices.payment_method, Newsagency_DB.Invoices.order_id From Newsagency_DB.Invoices inner join Newsagency_DB.Orders on Newsagency_DB.Invoices.order_id = Newsagency_DB.Orders.order_id inner join Newsagency_DB.Customers on Newsagency_DB.Orders.customer_id = Newsagency_DB.Customers.customer_id Where Newsagency_DB.Customers.lastName = ?;", new String [] {lastName});
		return resultSet;
	}
		
	public static ResultSet searchInvoiceByOrderID(String orderID, DBUtil db) throws HeadlessException, IllegalOrderID, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
		ResultSet resultSet = null;
		resultSet = db.executeQuery("Select Newsagency_DB.Orders.order_id, Newsagency_DB.Invoices.invoice_id, Newsagency_DB.Invoices.invoice_date, Newsagency_DB.Invoices.payment_method, Newsagency_DB.Invoices.order_id From Newsagency_DB.Invoices inner join Newsagency_DB.Orders on Newsagency_DB.Invoices.order_id = Newsagency_DB.Orders.order_id Where Newsagency_DB.Orders.order_id = ?;", new String [] {orderID});
		return resultSet;
	}
	
	public static ResultSet searchInvoiceByDate(String dateString,DBUtil db) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
		ResultSet resultSet = null;
		if(dateString != null) {
			String [] params = new String [] {dateString};
			String sqlString = "Select * from Invoices where date_format(invoice_date,'%Y-%m-%d') = ?;";
			resultSet = db.executeQuery(sqlString, params);
		}
			return resultSet;

	}
	
	public static ResultSet searchInvoiceByMonth(String monthString, DBUtil db) throws SQLException
	{
		return db.executeQuery("Select invoice_id, invoice_date, payment_method, order_id From Invoices Where MONTH(invoice_date) = ?", new String [] {monthString});
	}
	
	public static int setInvoiceOverdue(String invoiceID, DBUtil db) throws SQLException
	{
		int sign = 0;
		sign = db.executeUpdate("Update Invoices Set isOverdue = 'Yes' Where invoice_id = ?", new String [] {invoiceID});
		return sign;
	}
	
	public static int setInvoicePaidFor(String invoiceID, DBUtil db) throws SQLException
	{
		int sign = 0;
		sign = db.executeUpdate("Update Invoices Set isPaidFor = 'Yes' Where invoice_id = ?", new String [] {invoiceID});
		return sign;
	}
	
	public static ResultSet searchInvoicesPaidFor(DBUtil db) throws SQLException
	{
		return db.executeQuery1("Select invoice_id, invoice_date, payment_method, order_id From Invoices Where isPaidFor Is Not Null;");
	}
	
	public static void printInvoiceSummary(DBUtil db) throws SQLException, IOException
	{
		ResultSet resultSet = searchInvoicesPaidFor(db);
		
		String fileName = "paid_invoice_summary_files/paid_invoice_summary_" + new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss").format(new Date()) + ".txt";
		File file = new File(fileName);
		file.createNewFile();
		
		PrintWriter printWriter = new PrintWriter(new FileOutputStream(file,true));
		printWriter.write("Invoice summary\n---\n");
		printWriter.println("Paid Invoices");
		traverseResultSet(printWriter, resultSet);
		printWriter.flush();
		printWriter.close();
	}
	
	public static void printMonthlyInvoiceSummary(DBUtil db, String monthString) throws SQLException, IOException
	{
		
		HashMap<String, Integer> monthToNumber = new HashMap<String, Integer>();
		String[] months = new DateFormatSymbols().getMonths();
		
		for (int i = 0; i < months.length; i++) {
			monthToNumber.put(months[i], i+1);
		}
		
		String monthNumber = monthToNumber.get(monthString).toString();
		ResultSet resultSet = searchInvoiceByMonth(monthNumber, db);
		
		String fileName = "monthly_invoice_summary_files/Invoice_Summary_For_" + monthString + ".txt";
		File file = new File(fileName);
		file.createNewFile();
		
		PrintWriter printWriter = new PrintWriter(new FileOutputStream(file,true));
		printWriter.write("Invoice summary\n---\n");
		printWriter.println("Monthly Invoices");
		traverseResultSet(printWriter, resultSet);
		printWriter.flush();
		printWriter.close();
	}
	
	static void traverseResultSet(PrintWriter printWriter,ResultSet resultSet) throws SQLException {
		printWriter.write("  " + "Invoice ID" + "   "  + "Invoice Date" +  "   " + "Payment Method" + "   " + "Order ID\n");
		while (resultSet.next()) { 
			printWriter.write("- " + resultSet.getString(1) + "      " + resultSet.getString(2) + "     " + resultSet.getString(3) + "             " + resultSet.getString(4) + "\n");
		}
	}
}
