package core;

import exceptions.CustomerException;

public class Customer {

	private String customerId;
	private String firstName;
	private String lastName;
	private String phoneNumber;
	private String deliveryArea;
	private String address;

	public Customer() {
		this.customerId = generateId();
	}

	private String generateId() {
		return "C" + String.valueOf(System.currentTimeMillis()).substring(7, 13);
	}

	public String getCustomerId() {
		return customerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getDeliveryArea() {
		return deliveryArea;
	}

	public void setDeliveryArea(String deliveryArea) {
		this.deliveryArea = deliveryArea;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String[] getAttributes() {
		return new String[] { this.customerId, this.firstName, this.lastName, this.phoneNumber, this.deliveryArea, this.address };
	}

	public static boolean validateName(String name) throws CustomerException {
		boolean valid = false;
		if (name.length() < 2) {
			throw new CustomerException("Name cannot be less than 2 characters");
		} else if (name.length() >= 2 && name.length() <= 30) {
			valid = true;
		} else if (name.length() > 30) {
			throw new CustomerException("Name cannot be longer than 30 characters");
		}
		return valid;
	}

	public static boolean validateAddress(String address) throws CustomerException {
		boolean valid = false;
		if (!address.matches("[A-Z0-9]{3}\\s[A-Z0-9]{4}")) {
			throw new CustomerException("Not a valid Eircode");
		} else {
			valid = true;
		}
		return valid;
	}

	public static boolean validatePhoneNumber(String phone) throws CustomerException {
		boolean valid = false;
		if (!phone.matches("08\\d{8}")) {
			throw new CustomerException("Not a valid phone number");
		} else {
			valid = true;
		}
		return valid;
	}
	
	public static boolean validateDeliveryArea(String deliveryArea) throws CustomerException {
		boolean valid = false;
		if (deliveryArea.length() < 5) {
			throw new CustomerException("Delivery area cannot be less than 5 characters");
		} else if (deliveryArea.length() > 35) {
			throw new CustomerException("Delivery area cannot be more than 35 characters");
		} else {
			valid = true;
		}
		return valid;
	}

}
