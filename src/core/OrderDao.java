package core;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import db.DBUtil;
import exceptions.IllegalOrderID;
import exceptions.IllegalOrderItemLength;
import exceptions.IllegalOrderPrice;
import exceptions.WrongCustomerIDFormatException;

public interface OrderDao {
	public static int generateOrder(DBUtil dbutil, Order order) throws SQLException, WrongCustomerIDFormatException, IllegalOrderID, IllegalOrderItemLength, IllegalOrderPrice {
		Order.validateCustomerId(order.getCustomerId());
		Order.validateDescription(order.getDescription());
		Order.validateOrderID(order.getOrderId());
		Order.validatePrice(order.getPrice());
		int sign = 0;
		sign = dbutil.executeUpdate("insert into orders(order_id,customer_id,order_date,order_description,order_price) values(?,?,?,?,?)",order.getAttributesArray1());
		return sign;
	}
	
	public static ResultSet searchAllOrders(DBUtil dbutil) throws SQLException {
		return dbutil.executeQuery1("select * from orders");
	}

	public static ResultSet searchOrderByOrderID(DBUtil dbUtil, String orderID) throws SQLException {
		ResultSet resultSet = null;
		resultSet = dbUtil.executeQuery("select * from Orders where order_id = ?", new String[] {orderID});
		return resultSet;
	}

	public static ResultSet searchOrderByName(String firstName, String lastName, DBUtil dbUtil)throws SQLException {
		ResultSet resultSet = null;
		String[] params = new String[] { firstName, lastName };
		String sqlString = "select * from (select Customers.firstName,Customers.lastName,Orders.order_id,Orders.customer_id,Orders.order_date,Orders.order_description,Orders.order_price from Orders inner join Customers on Orders.customer_id = Customers.customer_id)as orders_customer where orders_customer.firstName = ? and orders_customer.lastName = ?";
		resultSet = dbUtil.executeQuery(sqlString, params);
		return resultSet;
	}
	public static ResultSet searchOrderByFirstName(String firstName, DBUtil dbUtil)throws SQLException {
		ResultSet resultSet = null;
		String sqlString = "select * from (select Customers.firstName,Customers.lastName,Orders.order_id,Orders.customer_id,Orders.order_date,Orders.order_description,Orders.order_price from Orders inner join Customers on Orders.customer_id = Customers.customer_id)as orders_customer where orders_customer.firstName = ?";
		resultSet = dbUtil.executeQuery(sqlString, new String[] {firstName});
		return resultSet;
	}
	public static ResultSet searchOrderByLastName(String lastName, DBUtil dbUtil)throws SQLException {
		ResultSet resultSet = null;
		String sqlString = "select * from (select Customers.firstName,Customers.lastName,Orders.order_id,Orders.customer_id,Orders.order_date,Orders.order_description,Orders.order_price from Orders inner join Customers on Orders.customer_id = Customers.customer_id)as orders_customer where orders_customer.lastName = ?";
		resultSet = dbUtil.executeQuery(sqlString, new String[] {lastName });
		return resultSet;
	}

	public static ResultSet searchOrderByDate(String dateString, DBUtil dbUtil)throws SQLException {
		ResultSet resultSet = null;
		String sqlString = "select * from Orders where date_format(order_date,'%Y/%m/%d') = ?";
		resultSet = dbUtil.executeQuery(sqlString, new String[] {dateString});
		return resultSet;
	}

	public static ResultSet searchDeliveredOrder(DBUtil dbUtil) throws SQLException {
		return dbUtil.executeQuery1("select order_id,order_description,order_price from Orders where isDelivered is not null");
	} 

	public static ResultSet searchUndeliveredOrder(DBUtil dbUtil) throws SQLException {
		return dbUtil.executeQuery1("select order_id,order_description,order_price from Orders where isDelivered is null");
	}

	public static ResultSet searchDispatchedOrder(DBUtil dbUtil) throws SQLException {
		return dbUtil.executeQuery1("select order_id,order_description,order_price from Orders where isDispatched is not null and isDelivered is null");
	}

	public static ResultSet searchUnfulfilledOrder(DBUtil dbUtil) throws SQLException {
		return dbUtil.executeQuery1("select order_id,order_description,order_price from Orders where isDispatched is null");
	}

	public static int editOrderDecriptionPrice(String order_id,String order_description,double order_price,DBUtil dbUtil) throws SQLException,IllegalOrderPrice, IllegalOrderItemLength {
		Order.validateDescription(order_description);
		Order.validatePrice(order_price);
		return dbUtil.executeUpdate("update Orders set order_description = ?,order_price = ? where order_id = ?", new String []{order_description,String.valueOf(order_price),order_id});
	}
	public static int editOrderDecription(String order_id,String order_description,DBUtil dbUtil) throws SQLException,IllegalOrderItemLength {
		Order.validateDescription(order_description);
		return dbUtil.executeUpdate("update Orders set order_description = ? where order_id = ?", new String []{order_description,order_id});
	}
	public static int editOrderPrice(String order_id,double order_price,DBUtil dbUtil) throws SQLException, IllegalOrderPrice{
		Order.validatePrice(order_price);
		return dbUtil.executeUpdate("update Orders set order_price = ? where order_id = ?", new String []{String.valueOf(order_price),order_id});
	}
	
	public static int setOrderActive(String order_id,DBUtil dbUtil) throws SQLException {
		return dbUtil.executeUpdate("update Orders set isActive = 'Yes' where order_id = ?", new String []{order_id});
	}
	
	public static int setOrderInactive(String order_id,DBUtil dbUtil) throws SQLException {
		return dbUtil.executeUpdate("update Orders set isActive = null where order_id = ?", new String []{order_id});
	}

	public static void printOutOrderSummary(DBUtil dbUtil) throws SQLException, IOException {
		ResultSet resultSet = searchDispatchedOrder(dbUtil);
		ResultSet resultSet2 = searchUnfulfilledOrder(dbUtil);
		ResultSet resultSet3 = searchDeliveredOrder(dbUtil);

		String fileName = "files/order_summary_" + new SimpleDateFormat("yy_MM_dd_HH_mm_ss").format(new Date()) + ".txt";
		File file = new File(fileName);
		file.createNewFile(); 
		PrintWriter printWriter = new PrintWriter(new FileOutputStream(file,true));
		printWriter.write("Order summary\n---\n");
		printWriter.println("Unfulfilled orders:");
		traverseResultSet(printWriter, resultSet2);
		printWriter.println();
		printWriter.write("Dispatched orders:\n");
		traverseResultSet(printWriter, resultSet);
		printWriter.println();
		printWriter.write("Delivered orders:\n");
		traverseResultSet(printWriter, resultSet3);
		printWriter.flush();
		printWriter.close();
	} 

	static void traverseResultSet(PrintWriter printWriter,ResultSet resultSet) throws SQLException {
		printWriter.write("  " + "Order ID" + "   "  + "Description" +  "   " +  "Price\n");
		while (resultSet.next()) { 
			printWriter.write("- " + resultSet.getString(1) + "   " + resultSet.getString(2) + "   " + resultSet.getString(3) + "\n");
		}
	}
}
