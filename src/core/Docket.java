package core;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import db.DBUtil;

public interface Docket {

	public static boolean generate(DBUtil dbUtil) throws Exception {

		ResultSet rs = dbUtil.executeQuery1(
				"SELECT orders.order_date, customers.firstName,customers.address,customers.phone_no, orders.order_id,orders.order_description,\r\n"
						+ "orders.order_price\r\n" + "FROM Orders\r\n" + "INNER JOIN customers\r\n"
						+ "ON orders.customer_id= customers.customer_id WHERE ISDispatched = 'YES';");

		ArrayList odate = new ArrayList();
		ArrayList name = new ArrayList();
		ArrayList address = new ArrayList();
		ArrayList num = new ArrayList();
		ArrayList orid = new ArrayList();
		ArrayList desc = new ArrayList();
		ArrayList price = new ArrayList();
		
	
		while (rs.next()) {
			String date = rs.getString("order_date");
			String fname = rs.getString("firstName");
			String addre = rs.getString("address");
			String ohnum = rs.getString("phone_no");
			String oid = rs.getString("order_id");
			String orderdes = rs.getString("order_description");
			String oprice = rs.getString("order_price");

			odate.add(date);
			name.add(fname);
			address.add(addre);
			num.add(ohnum);
			orid.add(oid);
			desc.add(orderdes);
			price.add(oprice);
		}

		for (int i = 0; i < num.size(); i++) {
			String orderNumber = (String) orid.get(i);
			String filePath = String.format("dockets/docket_%s.txt", orderNumber);
			BufferedWriter writer = new BufferedWriter(new FileWriter(new File(filePath)));
			String timeStamp = new SimpleDateFormat("yyyy:MM:dd").format(Calendar.getInstance().getTime());

			writer.write("Order date: " + odate.get(i));
			writer.newLine();
			writer.write("Dispatch date: " + timeStamp);
			writer.newLine();
			writer.newLine();
			writer.write("Customer Name: " + name.get(i));
			writer.newLine();
			writer.write("Customer Address: " + address.get(i));
			writer.newLine();
			writer.write(String.format("Customer Phone Number: %s", num.get(i)));
			writer.newLine();
			String orderLine = String.format("Order ID: %s | Items: %s | Price: %s", orid.get(i), desc.get(i), price.get(i));
			writer.write(orderLine);
			UpdateIsDispatched(dbUtil,orderNumber);
			writer.close();
			
		}
		
		return true;

	}

	static void UpdateIsDispatched(DBUtil dbUtil, String orderNumber) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
		dbUtil.getConnection();
		dbUtil.executeUpdate("update orders set isDispatched = 'YES' where orders.order_id = ? ", new String [] {orderNumber});
		
	}

	static boolean generateByArea(DBUtil dbUtil, String area) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
		dbUtil.getConnection();
		ResultSet result = dbUtil.executeQuery(" SELECT orders.order_date,customers.firstName,customers.address,customers.phone_no,orders.order_id,orders.order_description,orders.order_price  FROM orders" + 
				" INNER JOIN customers ON orders.customer_id= customers.customer_id where orders.isActive = 'YES' AND customers.deliveryArea =? ", new String [] {area});
		if(!result.isBeforeFirst()) {
			return false;
		}
		ArrayList odate = new ArrayList();
		ArrayList name = new ArrayList();
		ArrayList address = new ArrayList();
		ArrayList num = new ArrayList();
		ArrayList orid = new ArrayList();
		ArrayList desc = new ArrayList();
		ArrayList price = new ArrayList();
		
	
		while (result.next()) {
			String date = result.getString("order_date");
			String fname = result.getString("firstName");
			String addre = result.getString("address");
			String ohnum = result.getString("phone_no");
			String oid = result.getString("order_id");
			String orderdes = result.getString("order_description");
			String oprice = result.getString("order_price");

			odate.add(date);
			name.add(fname);
			address.add(addre);
			num.add(ohnum);
			orid.add(oid);
			desc.add(orderdes);
			price.add(oprice);
		}

		for (int i = 0; i < num.size(); i++) {
			String orderNumber = (String) orid.get(i);
			String filePath = String.format("dockets/docket_%s.txt", orderNumber+"_"+area);
			BufferedWriter writer = new BufferedWriter(new FileWriter(new File(filePath)));
			String timeStamp = new SimpleDateFormat("yyyy:MM:dd").format(Calendar.getInstance().getTime());

			writer.write("Order date: " + odate.get(i));
			writer.newLine();
			writer.write("Dispatch date: " + timeStamp);
			writer.newLine();
			writer.newLine();
			writer.write("Customer Name: " + name.get(i));
			writer.newLine();
			writer.write("Customer Address: " + address.get(i));
			writer.newLine();
			writer.write(String.format("Customer Phone Number: %s", num.get(i)));
			writer.newLine();
			String orderLine = String.format("Order ID: %s | Items: %s | Price: %s", orid.get(i), desc.get(i), price.get(i));
			writer.write(orderLine);
			UpdateIsDispatched(dbUtil,orderNumber);
			writer.close();
			
		}
		
		return true;
		
		
	}
	

}
