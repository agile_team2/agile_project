package core;

import java.sql.ResultSet;
import java.sql.SQLException;

import db.DBUtil;
import exceptions.CustomerException;

public interface CustomerDao {

	public static boolean addCustomer(DBUtil dbutil, Customer customer) throws CustomerException, SQLException {
		boolean validate = false;
		if (Customer.validateName(customer.getFirstName()) && Customer.validateName(customer.getLastName())
				&& Customer.validateAddress(customer.getAddress())
				&& Customer.validateDeliveryArea(customer.getDeliveryArea())
				&& Customer.validatePhoneNumber(customer.getPhoneNumber())) {
			validate = true;
		}
		if (validate) {
			String[] values = customer.getAttributes();
			dbutil.executeUpdate(
					"insert into Customers(customer_id,firstName,lastName,phone_no,deliveryArea, address) values(?,?,?,?,?,?)",
					values);
		}
		return validate;
	}

	public static boolean editCustomerFirstName(DBUtil db, String customerId, String firstName)
			throws SQLException, CustomerException {
		boolean success = false;
		if (Customer.validateName(firstName)) {
			String[] data = { firstName, customerId };
			db.executeUpdate("update customers set firstName = ? where customer_id = ?;", data);
			success = true;
		}
		return success;
	}

	public static boolean editCustomerLastName(DBUtil db, String customerId, String lastName)
			throws SQLException, CustomerException {
		boolean success = false;
		if (Customer.validateName(lastName)) {
			String[] data = { lastName, customerId };
			db.executeUpdate("update customers set lastName = ? where customer_id = ?;", data);
			success = true;
		}
		return success;
	}

	public static boolean editCustomerPhoneNumber(DBUtil db, String customerId, String phoneNumber)
			throws SQLException, CustomerException {
		boolean success = false;
		if (Customer.validatePhoneNumber(phoneNumber)) {
			String[] data = { phoneNumber, customerId };
			db.executeUpdate("update customers set phone_no = ? where customer_id = ?;", data);
			success = true;
		}
		return success;
	}

	public static boolean editCustomerDeliveryArea(DBUtil db, String customerId, String deliveryArea)
			throws SQLException, CustomerException {
		boolean success = false;
		if (Customer.validateDeliveryArea(deliveryArea)) {
			String[] data = { deliveryArea, customerId };
			db.executeUpdate("update customers set deliveryArea = ? where customer_id = ?;", data);
			success = true;
		}
		return success;
	}

	public static boolean editCustomerEircode(DBUtil db, String customerId, String eircode)
			throws SQLException, CustomerException {
		boolean success = false;
		if (Customer.validateAddress(eircode)) {
			String[] data = { eircode, customerId };
			db.executeUpdate("update customers set address = ? where customer_id = ?;", data);
			success = true;
		}
		return success;
	}

	public static ResultSet getAllDeliveryAreas(DBUtil db) throws SQLException {
		return db.executeQuery1("select distinct deliveryArea from customers");
	}

	public static ResultSet searchAllCustomers(DBUtil dbutil) throws SQLException {
		return dbutil.executeQuery1("select * from customers");
	}
	
	public static ResultSet searchAllInactiveCustomers(DBUtil db) throws SQLException {
		return db.executeQuery1("select * from inactivecustomers");
	}

	public static ResultSet searchById(DBUtil dbutil, String id) throws SQLException {
		return dbutil.executeQuery("select * from customers where customer_id = ?", new String[] { id });
	}

	public static ResultSet searchByFirstName(DBUtil dbutil, String firstName) throws SQLException {
		return dbutil.executeQuery("select * from customers where firstName = ?", new String[] { firstName });
	}

	public static ResultSet searchByLastName(DBUtil dbutil, String lastName) throws SQLException {
		return dbutil.executeQuery("select * from customers where lastName = ?", new String[] { lastName });
	}

	public static ResultSet searchByEircode(DBUtil dbutil, String eircode) throws SQLException {
		return dbutil.executeQuery("select * from customers where address = ?", new String[] { eircode });
	}

	public static ResultSet searchByDeliveryArea(DBUtil dbutil, String deliveryArea) throws SQLException {
		return dbutil.executeQuery("select * from customers where deliveryArea = ?", new String[] { deliveryArea });
	}

	public static ResultSet searchByPhoneNumber(DBUtil dbutil, String phoneNumber) throws SQLException {
		return dbutil.executeQuery("select * from customers where phone_no = ?", new String[] { phoneNumber });
	}

	public static boolean setInactive(DBUtil db, String id) throws SQLException, CustomerException {
		ResultSet orders = db.executeQuery("select * from orders where customer_id = ?", new String[] { id });
		
		while (orders.next()) {
			String isActive = orders.getString(8);
			if (isActive != null) {
				throw new CustomerException("Customer has active orders");
			}
		}
		
		db.executeUpdate(
				"insert into InactiveCustomers select * from customers where customers.customer_id = ?;",
				new String[] { id });
		db.executeQuery1("SET FOREIGN_KEY_CHECKS = 0;");
		db.executeUpdate("delete from customers where customer_id = ?;", new String[] { id });
		db.executeQuery1("SET FOREIGN_KEY_CHECKS = 1;");
		return true;
	}

}
