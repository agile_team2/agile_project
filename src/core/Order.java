
package core;

import java.text.SimpleDateFormat;
import java.util.Date;

import exceptions.IllegalOrderPrice;
import exceptions.WrongCustomerIDFormatException;
import exceptions.IllegalOrderID;
import exceptions.IllegalOrderItemLength;

public class Order {
	private String orderId;
	private String customerId;
	private String date;
	private String description;
	private Double price;

	public Order() {
		this.orderId = Order.generateOrderIDBySystem();
		this.date = new SimpleDateFormat("yy/MM/dd HH:mm:ss").format(new Date());
	}

	public String getOrderId() {
		return orderId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public static String generateOrderIDBySystem() {
		return "O" + String.valueOf(System.currentTimeMillis()).substring(7,13);
	}

	public String[] getAttributesArray1() {
		return new String[] { this.orderId,this.customerId, this.date, this.description, String.valueOf(this.price) };
	}

	public static boolean validateCustomerId(String customerId) throws WrongCustomerIDFormatException {
		if (!customerId.matches("^C[0-9]{6}$")) {
			throw new WrongCustomerIDFormatException();
		}
		return true;
	}

	public static boolean validateDescription(String description) throws IllegalOrderItemLength {
		if (description.length() == 0 || description.length() > 256) {
			throw new IllegalOrderItemLength();
		}
		return true;
	}

	public static boolean validatePrice(double price) throws IllegalOrderPrice {
		if (price <= 0 || price > 1000) {
			throw new IllegalOrderPrice();
		}
		return true;
	}

	public static boolean validateOrderID(String orderID) throws IllegalOrderID {
		if (!orderID.matches("^O[0-9]{6}$")) {
			throw new IllegalOrderID();
		}
		return true;
	}
}
