package core;

import db.DBUtil;
import exceptions.InvalidInvoiceIDException;
import exceptions.InvalidPaymentMethodException;

public class Invoice
{
	private String invoiceID;
	private String invoiceDate;
	private String paymentMethod;
	private String isOverdue;
	private String isPaidFor;
	private String orderID;
	
	public Invoice()
	{
		this.invoiceID = generateId();
	}
	
	private String generateId() {
		return "I" + String.valueOf(System.currentTimeMillis()).substring(7, 13);
	}
	
	public void setIsPaidFor(String isPaidFor)
	{
		this.isPaidFor = isPaidFor;
	}
	
	public void setIsOverdue(String isOverdue)
	{
		this.isOverdue = isOverdue;
	}

	public void setInvoiceID(String invoiceID) {
		this.invoiceID = invoiceID;
	}
	
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	
	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}
	
	public String [] getAttributesArray1() {
		return new String [] {this.invoiceID,this.invoiceDate,this.paymentMethod,this.isOverdue,this.isPaidFor,this.orderID};
	}
	
	public boolean validatePaymentMethod() throws InvalidPaymentMethodException {
		String method  = this.getPaymentMethod();
		if(method.equals("Cheque") || method.equals("Cash")) {
			return true;
		}else {
			throw new InvalidPaymentMethodException();
		}
	}
	
	public boolean validateInvoiceID() throws InvalidInvoiceIDException {
		if(!this.invoiceID.matches("^I[0-9]{6}$")) {
			throw new InvalidInvoiceIDException();
		}
		return true;
	}
}
