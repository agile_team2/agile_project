package core;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;

import exceptions.EmptyFieldException;
import exceptions.NoFileExistException;

public class PrintDocket {

	public static boolean print(String docket) throws NoFileExistException, PrinterException {
		Path target = Paths.get("dockets/" + docket);
		if (Files.notExists(target)) {
			throw new NoFileExistException();
		}
		
		PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
		if (printServices.length == 0) {
			throw new PrinterException("No printers available");
		}
		
		PrinterJob job = PrinterJob.getPrinterJob();
		job.setPrintable(new DoPrint(docket));
		boolean doprint = job.printDialog();

		if (doprint) {

			job.print();

		}

		return doprint;

	}

}

class DoPrint implements Printable {
	String docket;

	public DoPrint(String print) {
		this.docket = print;
	}

	@Override
	public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
		if (pageIndex > 0) {
			return NO_SUCH_PAGE;
		}

		String content1 = null;
		Graphics2D g2d = (Graphics2D) graphics;
		int x = (int) pageFormat.getImageableX();
		int y = (int) pageFormat.getImageableY();
		g2d.translate(x, y);

		Font font = new Font("Serif", Font.PLAIN, 10);
		FontMetrics metrics = graphics.getFontMetrics(font);
		int lineHeight = metrics.getHeight();

		try {
			content1 = new String(Files.readAllBytes(Paths.get("dockets/" + docket)));

			BufferedReader br = new BufferedReader(new StringReader(content1));

			String line;
			// Just a safety net in case no margin was added.
			x += 50;
			y += 50;
			while ((line = br.readLine()) != null) {
				y += lineHeight;
				g2d.drawString(line, x, y);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block

		}

		return PAGE_EXISTS;
	}

}