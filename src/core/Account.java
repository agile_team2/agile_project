package core;

import java.sql.ResultSet;
import java.sql.SQLException;

import db.DBUtil;
import exceptions.EmptyFieldException;

public class Account {
	
	private String username;
	private String password;
	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	


	public static boolean login(DBUtil dbUtil, String userName, String password) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
		ResultSet rs = dbUtil.executeQuery("select username from users where username = ? and user_password = ?",
				new String[] { userName, password });
		boolean n = rs.next();
		
		String username = null;
		username = rs.getString(1);
		return username.equals(userName);

	}

	public boolean validateUsernamePassword() throws EmptyFieldException {
		String username = this.getUsername();
		String password = this.getPassword();
		if (username.equals("") || password.equals("")) {
			throw new EmptyFieldException();
			
		}
		return true; 
	}
}
