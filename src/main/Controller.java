package main;

import java.sql.SQLException;

import db.DBUtil;
import ui.LoginWindow;
import ui.MainWindow;

public class Controller {

	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
		DBUtil db = DBUtil.getDBUtilInstance();
		LoginWindow loginWindow = new LoginWindow();
		loginWindow.setDatabase(db);
	}

}
