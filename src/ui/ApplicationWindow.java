package ui;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.Window;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import com.mysql.cj.jdbc.result.ResultSetMetaData;

import core.Customer;
import core.CustomerDao;
import core.Order;
import core.OrderDao;
import core.PrintDocket;
import core.Docket;
import core.Invoice;
import core.InvoiceDao;
import db.DBUtil;
import exceptions.CustomerException;
import exceptions.IllegalOrderID;
import exceptions.IllegalOrderItemLength;
import exceptions.IllegalOrderPrice;
import exceptions.NoFileExistException;
import exceptions.WrongCustomerIDFormatException;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.UIManager;
import java.awt.GridLayout;
import java.awt.HeadlessException;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JTabbedPane;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.border.EtchedBorder;

public class ApplicationWindow {

	private static ApplicationWindow singleInstance = null;
	private DBUtil db;

	private JFrame frame;
	private static JTable table;
	private JTextField firstNameField;
	private JTextField lastNameField;
	private JTextField phoneNumberField;
	private JTextField deliveryAreaField;
	private JTextField eircodeField;
	private JButton btnSearchAll;
	private JTextField customerIdField;
	private JTextField orderCustIdField;
	private JTextField descField;
	private JTextField priceField;
	private JTextField searchOrderIdField;
	private JTextField orderDateField;
	private JTextField orderFirstNameField;
	private JTextField orderLastNameField;
	private JTextField editOrderID;
	private JTextField editOrderDesc;
	private JTextField editOrderPrice;
	private JTextField invoiceOrderIdField;
	private JTextField invoiceDateField;
	private JTextField invoiceOrderIdSearch;
	private JTextField invoiceDateSearch;
	private JTextField invoiceFirstName;
	private JTextField invoiceLastName;
	private JTextField invoiceIdField;
	private JComboBox<String> comboBox;

	/**
	 * Create the application.
	 */
	private ApplicationWindow() {
		initialize();
	}

	public static ApplicationWindow getInstance() {
		if (singleInstance == null) {
			singleInstance = new ApplicationWindow();
		}
		return singleInstance;
	}

	public void setDatabase(DBUtil db) {
		this.db = db;
	}

	public static DefaultTableModel buildTableModel(ResultSet rs) throws SQLException {

		ResultSetMetaData metaData = (ResultSetMetaData) rs.getMetaData();

		// names of columns
		Vector<String> columnNames = new Vector<String>();
		int columnCount = metaData.getColumnCount();
		for (int column = 1; column <= columnCount; column++) {
			columnNames.add(metaData.getColumnName(column));
		}

		// data of the table
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		while (rs.next()) {
			Vector<Object> vector = new Vector<Object>();
			for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
				vector.add(rs.getObject(columnIndex));
			}
			data.add(vector);
		}

		return new DefaultTableModel(data, columnNames);

	}

	public static void showErrorDialog(String err) {
		JOptionPane.showMessageDialog(null, err, "Warning", JOptionPane.WARNING_MESSAGE);
	}

	public static void showInfoDialog(String msg) {
		JOptionPane.showMessageDialog(null, msg, "Message", JOptionPane.INFORMATION_MESSAGE);
	}

	public static void updateTable(ResultSet rs) throws SQLException {
		table.setModel(buildTableModel(rs));
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1029, 739);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		centreWindow(frame);
		frame.getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(new LineBorder(new Color(64, 64, 64)), "Output", TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		panel.setBounds(341, 11, 682, 225);
		frame.getContentPane().add(panel);
		panel.setLayout(null);

		table = new JTable();
		table.setBounds(6, 17, 670, 197);
		panel.add(table);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(6, 6, 334, 713);
		frame.getContentPane().add(tabbedPane);

		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Customer", null, panel_1, null);
		panel_1.setLayout(null);

		JLabel lblFirstName = new JLabel("First Name");
		lblFirstName.setBounds(6, 6, 99, 28);
		panel_1.add(lblFirstName);

		firstNameField = new JTextField();
		firstNameField.setBounds(117, 7, 168, 26);
		panel_1.add(firstNameField);
		firstNameField.setColumns(10);

		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setBounds(6, 46, 99, 28);
		panel_1.add(lblLastName);

		lastNameField = new JTextField();
		lastNameField.setColumns(10);
		lastNameField.setBounds(117, 47, 168, 26);
		panel_1.add(lastNameField);

		JLabel lblPhoneNumber = new JLabel("Phone Number");
		lblPhoneNumber.setBounds(6, 86, 99, 28);
		panel_1.add(lblPhoneNumber);

		phoneNumberField = new JTextField();
		phoneNumberField.setColumns(10);
		phoneNumberField.setBounds(117, 87, 168, 26);
		panel_1.add(phoneNumberField);

		JLabel lblDeliveryArea = new JLabel("Delivery Area");
		lblDeliveryArea.setBounds(6, 126, 99, 28);
		panel_1.add(lblDeliveryArea);

		deliveryAreaField = new JTextField();
		deliveryAreaField.setColumns(10);
		deliveryAreaField.setBounds(117, 127, 168, 26);
		panel_1.add(deliveryAreaField);

		JLabel lblAddress = new JLabel("Eircode");
		lblAddress.setBounds(6, 166, 99, 28);
		panel_1.add(lblAddress);

		eircodeField = new JTextField();
		eircodeField.setColumns(10);
		eircodeField.setBounds(117, 167, 168, 26);
		panel_1.add(eircodeField);

		JButton btnNewCustomer = new JButton("Add new customer");
		btnNewCustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String firstName = firstNameField.getText();
				String lastName = lastNameField.getText();
				String eircode = eircodeField.getText();
				String phoneNumber = phoneNumberField.getText();
				String deliveryArea = deliveryAreaField.getText();
				Customer customer = new Customer();
				customer.setFirstName(firstName);
				customer.setLastName(lastName);
				customer.setAddress(eircode);
				customer.setPhoneNumber(phoneNumber);
				customer.setDeliveryArea(deliveryArea);
				try {
					CustomerDao.addCustomer(db, customer);
					ResultSet rs = CustomerDao.searchByEircode(db, eircode);
					table.setModel(buildTableModel(rs));
					firstNameField.setText("");
					lastNameField.setText("");
					eircodeField.setText("");
					phoneNumberField.setText("");
					deliveryAreaField.setText("");
				} catch (CustomerException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage(), "Warning", JOptionPane.WARNING_MESSAGE);
				} catch (SQLException sqlErr) {
					sqlErr.printStackTrace();
				}
			}
		});
		btnNewCustomer.setBounds(117, 196, 168, 29);
		panel_1.add(btnNewCustomer);

		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0)), "Search by", TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		panel_4.setBounds(6, 224, 301, 158);
		panel_1.add(panel_4);

		JButton btnNewButton = new JButton("First Name");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String firstName = firstNameField.getText();
				try {
					ResultSet rs = CustomerDao.searchByFirstName(db, firstName);
					table.setModel(buildTableModel(rs));
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		panel_4.add(btnNewButton);

		JButton btnNewButton_4 = new JButton("Last Name");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String lastName = lastNameField.getText();
				try {
					ResultSet rs = CustomerDao.searchByLastName(db, lastName);
					table.setModel(buildTableModel(rs));
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		panel_4.add(btnNewButton_4);

		JButton btnNewButton_2 = new JButton("Phone Number");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String phoneNumber = phoneNumberField.getText();
				try {
					ResultSet rs = CustomerDao.searchByPhoneNumber(db, phoneNumber);
					table.setModel(buildTableModel(rs));
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		panel_4.add(btnNewButton_2);

		JButton btnNewButton_1 = new JButton("Delivery Area");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String deliveryArea = deliveryAreaField.getText();
				try {
					ResultSet rs = CustomerDao.searchByDeliveryArea(db, deliveryArea);
					table.setModel(buildTableModel(rs));
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		panel_4.add(btnNewButton_1);

		JButton btnNewButton_3 = new JButton("Eircode");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String eircode = eircodeField.getText();
				try {
					ResultSet rs = CustomerDao.searchByEircode(db, eircode);
					table.setModel(buildTableModel(rs));
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		panel_4.add(btnNewButton_3);

		JButton btnSearchAll = new JButton("All");
		btnSearchAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					ResultSet rs = CustomerDao.searchAllCustomers(db);
					updateTable(rs);
				} catch (SQLException err) {
					// TODO Auto-generated catch block
					showErrorDialog(err.getMessage());
				}
			}
		});
		panel_4.add(btnSearchAll);
		
		JButton btnInactive = new JButton("Inactive");
		btnInactive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					ResultSet rs = CustomerDao.searchAllInactiveCustomers(db);
					updateTable(rs);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		panel_4.add(btnInactive);

		JPanel panel_5 = new JPanel();
		panel_5.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0)), "Edit", TitledBorder.LEADING,
				TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_5.setBounds(6, 394, 301, 196);
		panel_1.add(panel_5);
		panel_5.setLayout(null);

		JButton button_1 = new JButton("Last Name");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String id = customerIdField.getText();
				String lastName = lastNameField.getText();
				try {
					CustomerDao.editCustomerLastName(db, id, lastName);
					ResultSet rs = CustomerDao.searchById(db, id);
					updateTable(rs);
				} catch (SQLException err) {
					// TODO Auto-generated catch block
					err.printStackTrace();
				} catch (CustomerException e1) {
					// TODO Auto-generated catch block
					showErrorDialog(e1.getMessage());
				}
			}
		});
		button_1.setBounds(136, 59, 110, 29);
		panel_5.add(button_1);

		JButton button_2 = new JButton("Phone Number");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String id = customerIdField.getText();
				String phoneNumber = phoneNumberField.getText();
				try {
					CustomerDao.editCustomerPhoneNumber(db, id, phoneNumber);
					ResultSet rs = CustomerDao.searchById(db, id);
					updateTable(rs);
				} catch (SQLException err) {
					// TODO Auto-generated catch block
					err.printStackTrace();
				} catch (CustomerException e1) {
					// TODO Auto-generated catch block
					showErrorDialog(e1.getMessage());
				}
			}
		});
		button_2.setBounds(6, 84, 136, 29);
		panel_5.add(button_2);

		JButton button_3 = new JButton("Delivery Area");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String id = customerIdField.getText();
				String deliveryArea = deliveryAreaField.getText();
				try {
					CustomerDao.editCustomerDeliveryArea(db, id, deliveryArea);
					ResultSet rs = CustomerDao.searchById(db, id);
					updateTable(rs);
				} catch (SQLException err) {
					// TODO Auto-generated catch block
					err.printStackTrace();
				} catch (CustomerException e1) {
					// TODO Auto-generated catch block
					showErrorDialog(e1.getMessage());
				}
			}
		});
		button_3.setBounds(6, 112, 136, 29);
		panel_5.add(button_3);

		JButton button = new JButton("First Name");
		button.setBounds(6, 59, 136, 29);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String id = customerIdField.getText();
				String firstName = firstNameField.getText();
				try {
					CustomerDao.editCustomerFirstName(db, id, firstName);
					ResultSet rs = CustomerDao.searchById(db, id);
					updateTable(rs);
				} catch (SQLException err) {
					// TODO Auto-generated catch block
					err.printStackTrace();
				} catch (CustomerException e1) {
					// TODO Auto-generated catch block
					showErrorDialog(e1.getMessage());
				}
			}
		});
		panel_5.add(button);

		JButton button_4 = new JButton("Eircode");
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String id = customerIdField.getText();
				String eircode = eircodeField.getText();
				try {
					CustomerDao.editCustomerEircode(db, id, eircode);
					ResultSet rs = CustomerDao.searchById(db, id);
					updateTable(rs);
				} catch (SQLException err) {
					// TODO Auto-generated catch block
					err.printStackTrace();
				} catch (CustomerException e1) {
					// TODO Auto-generated catch block
					showErrorDialog(e1.getMessage());
				}
			}
		});
		button_4.setBounds(139, 112, 90, 29);
		panel_5.add(button_4);

		customerIdField = new JTextField();
		customerIdField.setBounds(136, 21, 159, 26);
		panel_5.add(customerIdField);
		customerIdField.setColumns(10);

		JLabel lblCustomerId = new JLabel("Customer ID");
		lblCustomerId.setBounds(17, 26, 107, 16);
		panel_5.add(lblCustomerId);
		
		JButton btnSetInactive = new JButton("Set inactive");
		btnSetInactive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String id = customerIdField.getText();
				
				try {
					CustomerDao.setInactive(db, id);
					showInfoDialog(String.format("Customer %s is now inactive", id));
				} catch (SQLException e1) {
					e1.printStackTrace();
				} catch (CustomerException e2) {
					// TODO Auto-generated catch block
					showErrorDialog(e2.getMessage());
				}
			}
		});
		btnSetInactive.setBounds(6, 161, 117, 29);
		panel_5.add(btnSetInactive);

		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Order", null, panel_2, null);
		panel_2.setLayout(null);

		JLabel lblCustomerId_1 = new JLabel("Customer ID");
		lblCustomerId_1.setBounds(6, 6, 100, 16);
		panel_2.add(lblCustomerId_1);

		orderCustIdField = new JTextField();
		orderCustIdField.setBounds(118, 1, 189, 26);
		panel_2.add(orderCustIdField);
		orderCustIdField.setColumns(10);

		JLabel lblDescription = new JLabel("Description");
		lblDescription.setBounds(6, 34, 100, 16);
		panel_2.add(lblDescription);

		descField = new JTextField();
		descField.setBounds(118, 29, 189, 26);
		panel_2.add(descField);
		descField.setColumns(10);

		JLabel lblPrice = new JLabel("Price");
		lblPrice.setBounds(6, 62, 100, 16);
		panel_2.add(lblPrice);

		priceField = new JTextField();
		priceField.setBounds(118, 57, 189, 26);
		panel_2.add(priceField);
		priceField.setColumns(10);

		JButton btnNewButton_5 = new JButton("Place order");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String customerID = orderCustIdField.getText();
				String orderDescription = descField.getText();
				String orderPrice = priceField.getText();
				Order order = new Order();
				order.setCustomerId(customerID);
				order.setDescription(orderDescription);
				order.setPrice(Double.parseDouble(orderPrice));
				try {
					OrderDao.generateOrder(db, order);
					ResultSet rs = OrderDao.searchAllOrders(db);
					updateTable(rs);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (WrongCustomerIDFormatException e1) {
					// TODO Auto-generated catch block
					showErrorDialog(e1.getMessage());
				} catch (IllegalOrderID e1) {
					// TODO Auto-generated catch block
					showErrorDialog(e1.getMessage());
				} catch (IllegalOrderItemLength e1) {
					// TODO Auto-generated catch block
					showErrorDialog(e1.getMessage());
				} catch (IllegalOrderPrice e1) {
					// TODO Auto-generated catch block
					showErrorDialog(e1.getMessage());
				}
			}
		});
		btnNewButton_5.setBounds(118, 87, 189, 29);
		panel_2.add(btnNewButton_5);

		JPanel panel_6 = new JPanel();
		panel_6.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0)), "Search by", TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		panel_6.setBounds(6, 117, 301, 230);
		panel_2.add(panel_6);
		panel_6.setLayout(null);

		JLabel lblOrderId = new JLabel("Order ID");
		lblOrderId.setBounds(26, 20, 61, 16);
		panel_6.add(lblOrderId);

		searchOrderIdField = new JTextField();
		searchOrderIdField.setBounds(115, 15, 180, 26);
		panel_6.add(searchOrderIdField);
		searchOrderIdField.setColumns(10);

		JButton btnNewButton_6 = new JButton("First Name");
		btnNewButton_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String firstName = orderFirstNameField.getText();
				try {
					ResultSet rs = OrderDao.searchOrderByFirstName(firstName, db);
					updateTable(rs);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnNewButton_6.setBounds(6, 142, 140, 29);
		panel_6.add(btnNewButton_6);

		JButton btnNewButton_7 = new JButton("Last Name");
		btnNewButton_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String lastName = orderLastNameField.getText();
				try {
					ResultSet rs = OrderDao.searchOrderByLastName(lastName, db);
					updateTable(rs);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnNewButton_7.setBounds(158, 142, 137, 29);
		panel_6.add(btnNewButton_7);

		JLabel lblDate = new JLabel("Date");
		lblDate.setBounds(26, 48, 61, 16);
		panel_6.add(lblDate);

		orderDateField = new JTextField();
		orderDateField.setBounds(115, 39, 180, 26);
		panel_6.add(orderDateField);
		orderDateField.setColumns(10);

		JButton btnOrderId = new JButton("Order ID");
		btnOrderId.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String id = searchOrderIdField.getText();
				try {
					ResultSet rs = OrderDao.searchOrderByOrderID(db, id);
					updateTable(rs);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnOrderId.setBounds(6, 170, 140, 29);
		panel_6.add(btnOrderId);

		JButton btnNewButton_8 = new JButton("Date");
		btnNewButton_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String date = orderDateField.getText();
				try {
					ResultSet rs = OrderDao.searchOrderByDate(date, db);
					updateTable(rs);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnNewButton_8.setBounds(158, 170, 137, 29);
		panel_6.add(btnNewButton_8);

		JLabel lblFirstName_1 = new JLabel("First name");
		lblFirstName_1.setBounds(26, 76, 81, 16);
		panel_6.add(lblFirstName_1);

		JLabel lblLastName_1 = new JLabel("Last name");
		lblLastName_1.setBounds(26, 104, 81, 16);
		panel_6.add(lblLastName_1);

		orderFirstNameField = new JTextField();
		orderFirstNameField.setBounds(115, 71, 180, 26);
		panel_6.add(orderFirstNameField);
		orderFirstNameField.setColumns(10);

		orderLastNameField = new JTextField();
		orderLastNameField.setBounds(115, 99, 180, 26);
		panel_6.add(orderLastNameField);
		orderLastNameField.setColumns(10);

		JButton btnNewButton_9 = new JButton("All orders");
		btnNewButton_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ResultSet rs;
				try {
					rs = OrderDao.searchAllOrders(db);
					updateTable(rs);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnNewButton_9.setBounds(89, 196, 117, 29);
		panel_6.add(btnNewButton_9);

		JPanel panel_7 = new JPanel();
		panel_7.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0)), "Edit", TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		panel_7.setBounds(6, 359, 301, 198);
		panel_2.add(panel_7);
		panel_7.setLayout(null);

		JLabel label = new JLabel("Order ID");
		label.setBounds(17, 28, 53, 16);
		panel_7.add(label);

		editOrderID = new JTextField();
		editOrderID.setBounds(106, 23, 180, 26);
		editOrderID.setColumns(10);
		panel_7.add(editOrderID);

		JLabel lblDescription_1 = new JLabel("Description");
		lblDescription_1.setBounds(17, 61, 92, 16);
		panel_7.add(lblDescription_1);

		editOrderDesc = new JTextField();
		editOrderDesc.setColumns(10);
		editOrderDesc.setBounds(106, 56, 180, 26);
		panel_7.add(editOrderDesc);

		JLabel lblPrice_1 = new JLabel("Price");
		lblPrice_1.setBounds(17, 94, 61, 16);
		panel_7.add(lblPrice_1);

		editOrderPrice = new JTextField();
		editOrderPrice.setColumns(10);
		editOrderPrice.setBounds(106, 89, 180, 26);
		panel_7.add(editOrderPrice);

		JButton btnDescription = new JButton("Description");
		btnDescription.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String id = editOrderID.getText();
				String description = editOrderDesc.getText();
				System.out.print(description);
				try {
					OrderDao.editOrderDecription(description, id, db);
					ResultSet rs = OrderDao.searchOrderByOrderID(db, id);
					updateTable(rs);
				} catch (SQLException err) {
					// TODO Auto-generated catch block
					err.printStackTrace();
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					showErrorDialog(e1.getMessage());
				} catch (IllegalOrderItemLength e1) {
					// TODO Auto-generated catch block
					showErrorDialog(e1.getMessage());
				}
			}
		});
		btnDescription.setBounds(6, 122, 133, 29);
		panel_7.add(btnDescription);

		JButton btnPrice = new JButton("Price");
		btnPrice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String id = editOrderID.getText();
				String priceStr = editOrderPrice.getText();
				double price = Double.parseDouble(priceStr);
				try {
					OrderDao.editOrderPrice(id, price, db);
					ResultSet rs = OrderDao.searchOrderByOrderID(db, id);
					updateTable(rs);
				} catch (SQLException err) {
					// TODO Auto-generated catch block
					err.printStackTrace();
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					showErrorDialog(e1.getMessage());
				} catch (IllegalOrderPrice e1) {
					// TODO Auto-generated catch block
					showErrorDialog(e1.getMessage());
				}
			}
		});
		btnPrice.setBounds(151, 122, 144, 29);
		panel_7.add(btnPrice);
		
		JButton btnNewButton_12 = new JButton("Set inactive");
		btnNewButton_12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String id = editOrderID.getText();
				
				try {
					OrderDao.setOrderInactive(id, db);
					showInfoDialog(String.format("Order %s is now inactive", id));
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnNewButton_12.setBounds(6, 151, 133, 29);
		panel_7.add(btnNewButton_12);

		JButton btnGenerateDockets = new JButton("Generate dockets");
		btnGenerateDockets.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String deliveryArea = (String) comboBox.getSelectedItem();
					boolean success = Docket.generateByArea(db, deliveryArea);
					if (success) {
						showInfoDialog(String.format("Dockets for %s generated", deliveryArea));
					} else {
						showInfoDialog(String.format("No active orders associated with %s", deliveryArea));
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					showErrorDialog(e1.getMessage());
				}
			}
		});
		btnGenerateDockets.setBounds(6, 602, 150, 29);
		panel_2.add(btnGenerateDockets);

		JButton btnNewButton_10 = new JButton("Generate summary");
		btnNewButton_10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					OrderDao.printOutOrderSummary(db);
					showInfoDialog("Order summary generated");
				} catch (SQLException e1) {
					showErrorDialog(e1.getMessage());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					showErrorDialog(e1.getMessage());
				}
			}
		});
		btnNewButton_10.setBounds(6, 631, 150, 29);
		panel_2.add(btnNewButton_10);

		JButton btnNewButton_11 = new JButton("Print docket");
		btnNewButton_11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				File workingDirectory = new File(System.getProperty("user.dir"));
				chooser.setCurrentDirectory(workingDirectory);

				int retVal = chooser.showOpenDialog(null);

				if (retVal == JFileChooser.APPROVE_OPTION) {
					File selectedFile = chooser.getSelectedFile();
					String fileName = selectedFile.getName();

					try {
						PrintDocket.print(fileName);
					} catch (NoFileExistException e1) {
						// TODO Auto-generated catch block
						showErrorDialog(e1.getMessage());
					} catch (PrinterException e1) {
						// TODO Auto-generated catch block
						showErrorDialog(e1.getMessage());
					}
				}
			}
		});
		btnNewButton_11.setBounds(157, 631, 150, 29);
		panel_2.add(btnNewButton_11);
		
		comboBox = new JComboBox();
		comboBox.setBounds(6, 570, 150, 27);
		panel_2.add(comboBox);
		
		JButton btnNewButton_13 = new JButton("Load delivery areas");
		btnNewButton_13.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					ArrayList<String> deliveryAreasList = new ArrayList<String>();
					ResultSet rs = CustomerDao.getAllDeliveryAreas(db);
					while (rs.next()) {
						deliveryAreasList.add(rs.getString(1));
					}
					String[] deliveryAreas = deliveryAreasList.toArray(new String[deliveryAreasList.size()]);
					comboBox.setModel(new DefaultComboBoxModel<String>(deliveryAreas));
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});
		btnNewButton_13.setBounds(152, 569, 155, 29);
		panel_2.add(btnNewButton_13);

		JPanel panel_3 = new JPanel();
		tabbedPane.addTab("Invoice", null, panel_3, null);
		panel_3.setLayout(null);

		JLabel lblOrderId_1 = new JLabel("Order ID");
		lblOrderId_1.setBounds(6, 6, 61, 16);
		panel_3.add(lblOrderId_1);

		JLabel lblInvoiceDate = new JLabel("Invoice date");
		lblInvoiceDate.setBounds(6, 34, 88, 16);
		panel_3.add(lblInvoiceDate);

		JLabel lblPaymentMethod = new JLabel("Payment method");
		lblPaymentMethod.setBounds(6, 60, 112, 16);
		panel_3.add(lblPaymentMethod);

		invoiceOrderIdField = new JTextField();
		invoiceOrderIdField.setBounds(128, 1, 179, 26);
		panel_3.add(invoiceOrderIdField);
		invoiceOrderIdField.setColumns(10);

		invoiceDateField = new JTextField();
		invoiceDateField.setBounds(128, 29, 179, 26);
		panel_3.add(invoiceDateField);
		invoiceDateField.setColumns(10);

		JComboBox<String> paymentMethodBox = new JComboBox<String>();
		paymentMethodBox.setModel(new DefaultComboBoxModel(new String[] { "Cash", "Cheque" }));
		paymentMethodBox.setBounds(130, 56, 99, 27);
		panel_3.add(paymentMethodBox);

		JButton btnGenerateInvoice = new JButton("Generate invoice");
		btnGenerateInvoice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String invoiceDate = invoiceDateField.getText();
				String invoiceOrderID = invoiceOrderIdField.getText();
				String paymentMethod = (String) paymentMethodBox.getSelectedItem();
				Invoice invoice = new Invoice();
				invoice.setInvoiceDate(invoiceDate);
				invoice.setOrderID(invoiceOrderID);
				invoice.setPaymentMethod(paymentMethod);
				try {
					InvoiceDao.generateInvoice(db, invoice);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnGenerateInvoice.setBounds(128, 85, 179, 29);
		panel_3.add(btnGenerateInvoice);

		JPanel panel_8 = new JPanel();
		panel_8.setLayout(null);
		panel_8.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0)), "Search by", TitledBorder.LEADING,

				TitledBorder.TOP, null, null));
		panel_8.setBounds(6, 112, 301, 230);
		panel_3.add(panel_8);

		JLabel label_1 = new JLabel("Order ID");
		label_1.setBounds(26, 20, 61, 16);
		panel_8.add(label_1);

		invoiceOrderIdSearch = new JTextField();
		invoiceOrderIdSearch.setColumns(10);
		invoiceOrderIdSearch.setBounds(115, 15, 180, 26);
		panel_8.add(invoiceOrderIdSearch);

		JButton button_5 = new JButton("First Name");
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String firstName = invoiceFirstName.getText();
				try {
					ResultSet rs = InvoiceDao.searchInvoiceByCustomerFirstName(firstName, db);
					updateTable(rs);
				} catch (SQLException e1) {
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (InstantiationException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IllegalAccessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		button_5.setBounds(6, 142, 140, 29);
		panel_8.add(button_5);

		JButton button_6 = new JButton("Last Name");
		button_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String lastName = invoiceLastName.getText();
				try {
					ResultSet rs = InvoiceDao.searchInvoiceByCustomerLastName(lastName, db);
					updateTable(rs);
				} catch (SQLException e1) {
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (InstantiationException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IllegalAccessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		button_6.setBounds(158, 142, 137, 29);
		panel_8.add(button_6);

		JLabel label_2 = new JLabel("Date");
		label_2.setBounds(26, 48, 61, 16);
		panel_8.add(label_2);

		invoiceDateSearch = new JTextField();
		invoiceDateSearch.setColumns(10);
		invoiceDateSearch.setBounds(115, 39, 180, 26);
		panel_8.add(invoiceDateSearch);

		JButton button_7 = new JButton("Order ID");
		button_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String orderID = invoiceOrderIdSearch.getText();
				try {
					ResultSet rs = InvoiceDao.searchInvoiceByOrderID(orderID, db);
					updateTable(rs);
				} catch (SQLException e1) {
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (InstantiationException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IllegalAccessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (HeadlessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IllegalOrderID e1) {
					// TODO Auto-generated catch block
					showErrorDialog(e1.getMessage());
				}
			}
		});
		button_7.setBounds(6, 170, 140, 29);
		panel_8.add(button_7);

		JButton button_8 = new JButton("Date");
		button_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String dateString = invoiceDateSearch.getText();
				try {
					ResultSet rs = InvoiceDao.searchInvoiceByDate(dateString, db);
					updateTable(rs);
				} catch (SQLException e1) {
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (InstantiationException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IllegalAccessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		button_8.setBounds(158, 170, 137, 29);
		panel_8.add(button_8);

		JLabel label_3 = new JLabel("First name");
		label_3.setBounds(26, 76, 81, 16);
		panel_8.add(label_3);

		JLabel label_4 = new JLabel("Last name");
		label_4.setBounds(26, 104, 81, 16);
		panel_8.add(label_4);

		invoiceFirstName = new JTextField();
		invoiceFirstName.setColumns(10);
		invoiceFirstName.setBounds(115, 71, 180, 26);
		panel_8.add(invoiceFirstName);

		invoiceLastName = new JTextField();
		invoiceLastName.setColumns(10);
		invoiceLastName.setBounds(115, 99, 180, 26);
		panel_8.add(invoiceLastName);

		JButton btnAllInvoices = new JButton("All invoices");
		btnAllInvoices.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ResultSet rs;
				try {
					rs = InvoiceDao.searchAllInvoices(db);
					updateTable(rs);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});
		btnAllInvoices.setBounds(89, 196, 117, 29);
		panel_8.add(btnAllInvoices);
		
		JLabel lblNewLabel = new JLabel("Invoice ID");
		lblNewLabel.setBounds(33, 376, 85, 16);
		panel_3.add(lblNewLabel);
		
		invoiceIdField = new JTextField();
		invoiceIdField.setBounds(128, 371, 130, 26);
		panel_3.add(invoiceIdField);
		invoiceIdField.setColumns(10);
		
		JButton btnSetOverdue = new JButton("Set overdue");
		btnSetOverdue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String invoice = invoiceIdField.getText();
				
				try {
					InvoiceDao.setInvoiceOverdue(invoice, db);
					showInfoDialog(String.format("Invoice %s set as overdue", invoice));
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnSetOverdue.setBounds(138, 409, 117, 29);
		panel_3.add(btnSetOverdue);
		
		JButton btnNewButton_14 = new JButton("Paid invoice summary");
		btnNewButton_14.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					InvoiceDao.printInvoiceSummary(db);
					showInfoDialog("Paid invoice summaery generated");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					showErrorDialog(e1.getMessage());
				}
			}
		});
		btnNewButton_14.setBounds(6, 487, 162, 29);
		panel_3.add(btnNewButton_14);
		
		
		String[] months = new DateFormatSymbols().getMonths();
		
		JComboBox comboBox_1 = new JComboBox(months);
		comboBox_1.setBounds(6, 528, 216, 27);
		panel_3.add(comboBox_1);
		
		JButton btnNewButton_15 = new JButton("Monthly invoice summary");
		btnNewButton_15.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String month = (String) comboBox_1.getSelectedItem();
					InvoiceDao.printMonthlyInvoiceSummary(db, month);
					showInfoDialog(String.format("Invoice summary for %s generated", month));
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnNewButton_15.setBounds(6, 566, 223, 29);
		panel_3.add(btnNewButton_15);
		
		


		frame.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				db.closeAll();
			}
		});
	}

	public static void centreWindow(Window frame) {
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
		int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
		frame.setLocation(x, y);
	}
}
