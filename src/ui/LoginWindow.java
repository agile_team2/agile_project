package ui;

import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.Window;

import javax.swing.JFrame;

import db.DBUtil;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JTextField;

import core.Account;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class LoginWindow {

	private JFrame frame;
	private DBUtil db;
	private JTextField fieldUsername;
	private JTextField fieldPassword;

	/**
	 * Create the application.
	 */
	public LoginWindow() {
		initialize();
	}
	
	public void setDatabase(DBUtil db) {
		this.db = db;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 380, 200);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setVisible(true);
		
		JLabel labelUsername = new JLabel("Username");
		labelUsername.setBounds(54, 36, 79, 14);
		frame.getContentPane().add(labelUsername);
		
		fieldUsername = new JTextField();
		fieldUsername.setBounds(143, 33, 134, 20);
		frame.getContentPane().add(fieldUsername);
		fieldUsername.setColumns(10);
		
		JLabel labelPassword = new JLabel("Password");
		labelPassword.setBounds(54, 67, 69, 14);
		frame.getContentPane().add(labelPassword);
		
		fieldPassword = new JPasswordField();
		fieldPassword.setBounds(143, 64, 134, 20);
		frame.getContentPane().add(fieldPassword);
		fieldPassword.setColumns(10);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String username = fieldUsername.getText();
				String password = fieldPassword.getText();
				
				boolean success = false;
				try {
					success = Account.login(db, username, password);
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
					e.printStackTrace();
//					JOptionPane.showMessageDialog(null, "Login unsuccessful", "Warning!", JOptionPane.WARNING_MESSAGE);
				}
				if (success) {
					frame.setVisible(false);
					ApplicationWindow window = ApplicationWindow.getInstance();
					window.setDatabase(db);
				} else {
					JOptionPane.showMessageDialog(null, "Login unsuccessful", "Warning!", JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		btnLogin.setBounds(137, 107, 89, 23);
		frame.getContentPane().add(btnLogin);
		
		centreWindow(frame);
		
		frame.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				db.closeAll();
			}
		});
	}
	
	public static void centreWindow(Window frame) {
	    Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
	    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
	    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
	    frame.setLocation(x, y);
	}
}
