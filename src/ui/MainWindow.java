package ui;

import javax.swing.JFrame;

import db.DBUtil;
import exceptions.CustomerException;
import exceptions.IllegalOrderID;
import exceptions.IllegalOrderItemLength;
import exceptions.IllegalOrderPrice;
import exceptions.WrongCustomerIDFormatException;

import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JMenu;
import javax.swing.JRadioButtonMenuItem;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.CardLayout;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import com.mysql.cj.jdbc.result.ResultSetMetaData;

import core.Customer;
import core.CustomerDao;
import core.Docket;
import core.Invoice;
import core.InvoiceDao;
import core.Order;
import core.OrderDao;

import javax.swing.JTable;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class MainWindow {

	private static MainWindow singleInstance = null;
	private JFrame frame;
	private DBUtil db;
	private JTextField firstNameField;
	private JTextField lastNameField;
	private JTextField eircodeField;
	private JTextField phoneField;
	private JTable table;
	private JTextField customerOrderIDField;
	private JTextField orderDescriptionField;
	private JTextField orderPriceField;
	private JTextField customerOrderFirstNameField;
	private JTextField customerOrderLastNameField;
	private JTextField orderIDField;
	private JTextField orderDateField;
	private JTable orderTable;
	private JTable invoiceTable;
	private JTextField invoiceIDField;
	private JTextField invoiceDateField;
	private JTextField invoiceOrderIDField;
	private JTextField invoiceFirstNameField;
	private JTextField invoiceLastNameField;
	private JTextField invoiceOrderIDSearchField;
	private JTextField invoiceDateSearchField;

	/**
	 * Create the application.
	 */
	private MainWindow() {
		initialize();
	}
	
	public static MainWindow getInstance() {
		if (singleInstance == null) {
			singleInstance = new MainWindow();
		}
		return singleInstance;
	}

	public void setDatabase(DBUtil db) {
		this.db = db;
	}
	
	public static DefaultTableModel buildTableModel(ResultSet rs)
	        throws SQLException {

	    ResultSetMetaData metaData = (ResultSetMetaData) rs.getMetaData();

	    // names of columns
	    Vector<String> columnNames = new Vector<String>();
	    int columnCount = metaData.getColumnCount();
	    for (int column = 1; column <= columnCount; column++) {
	        columnNames.add(metaData.getColumnName(column));
	    }

	    // data of the table
	    Vector<Vector<Object>> data = new Vector<Vector<Object>>();
	    while (rs.next()) {
	        Vector<Object> vector = new Vector<Object>();
	        for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
	            vector.add(rs.getObject(columnIndex));
	        }
	        data.add(vector);
	    }

	    return new DefaultTableModel(data, columnNames);

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 640, 480);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JRadioButtonMenuItem rdbtnmntmExit = new JRadioButtonMenuItem("Exit");
		rdbtnmntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				db.closeAll();
				System.exit(0);
			}
		});
		mnFile.add(rdbtnmntmExit);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 45, 604, 348);
		frame.getContentPane().add(panel);
		panel.setLayout(new CardLayout(0, 0));
		
		JPanel customerCard = new JPanel();
		panel.add(customerCard, "customerCard");
		customerCard.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Customer Menu");
		lblNewLabel.setBounds(252, 5, 99, 14);
		customerCard.add(lblNewLabel);
		
		JLabel lblCustomerFirstName = new JLabel("Customer first name");
		lblCustomerFirstName.setBounds(10, 40, 137, 14);
		customerCard.add(lblCustomerFirstName);
		
		JLabel lblCustomerLastName = new JLabel("Customer last name");
		lblCustomerLastName.setBounds(10, 65, 137, 14);
		customerCard.add(lblCustomerLastName);
		
		JLabel lblCustomerEircode = new JLabel("Customer Eircode");
		lblCustomerEircode.setBounds(10, 90, 137, 14);
		customerCard.add(lblCustomerEircode);
		
		JLabel lblCustomerPhoneNumber = new JLabel("Customer phone number");
		lblCustomerPhoneNumber.setBounds(10, 115, 163, 14);
		customerCard.add(lblCustomerPhoneNumber);
		
		firstNameField = new JTextField();
		firstNameField.setBounds(183, 37, 137, 20);
		customerCard.add(firstNameField);
		firstNameField.setColumns(10);
		
		lastNameField = new JTextField();
		lastNameField.setBounds(183, 62, 137, 20);
		customerCard.add(lastNameField);
		lastNameField.setColumns(10);
		
		eircodeField = new JTextField();
		eircodeField.setBounds(183, 87, 137, 20);
		customerCard.add(eircodeField);
		eircodeField.setColumns(10);
		
		phoneField = new JTextField();
		phoneField.setBounds(183, 112, 137, 20);
		customerCard.add(phoneField);
		phoneField.setColumns(10);
		
		JButton btnAddCustomer = new JButton("Add customer");
		btnAddCustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String firstName = firstNameField.getText();
				String lastName = lastNameField.getText();
				String eircode = eircodeField.getText();
				String phoneNumber = phoneField.getText();
//				Customer customer = new Customer(firstName, lastName, phoneNumber, eircode);
				Customer customer = new Customer();
				customer.setFirstName(firstName);
				customer.setLastName(lastName);
				customer.setAddress(eircode);
				customer.setPhoneNumber(phoneNumber);
				try {
					CustomerDao.addCustomer(db, customer);
					ResultSet rs = CustomerDao.searchByEircode(db, eircode);
					table.setModel(buildTableModel(rs));
					firstNameField.setText("");
					lastNameField.setText("");
					eircodeField.setText("");
					phoneField.setText("");
				} catch (CustomerException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage(), "Warning", JOptionPane.WARNING_MESSAGE);
				} catch (SQLException sqlErr) {
					sqlErr.printStackTrace();
				}
			}
		});
		btnAddCustomer.setBounds(183, 138, 137, 23);
		customerCard.add(btnAddCustomer);
		
		JButton btnSearchCustomer = new JButton("Search by first name");
		btnSearchCustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String firstName = firstNameField.getText();
				try {
					ResultSet rs = CustomerDao.searchByFirstName(db, firstName);
					table.setModel(buildTableModel(rs));
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnSearchCustomer.setBounds(330, 36, 153, 23);
		customerCard.add(btnSearchCustomer);
		
		table = new JTable();
		table.setBounds(10, 172, 584, 165);
		customerCard.add(table);
		
		JButton btnSearchByLast = new JButton("Search by last name");
		btnSearchByLast.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String lastName = lastNameField.getText();
				try {
					ResultSet rs = CustomerDao.searchByLastName(db, lastName);
					table.setModel(buildTableModel(rs));
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnSearchByLast.setBounds(330, 61, 153, 23);
		customerCard.add(btnSearchByLast);
		
		JButton btnSearchByEircode = new JButton("Search by Eircode");
		btnSearchByEircode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String eircode = eircodeField.getText();
				try {
					ResultSet rs = CustomerDao.searchByEircode(db, eircode);
					table.setModel(buildTableModel(rs));
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnSearchByEircode.setBounds(330, 86, 153, 23);
		customerCard.add(btnSearchByEircode);
		
		JButton btnSearchByPhone = new JButton("Search by phone number");
		btnSearchByPhone.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String phoneNumber = phoneField.getText();
				try {
					ResultSet rs = CustomerDao.searchByPhoneNumber(db, phoneNumber);
					table.setModel(buildTableModel(rs));
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnSearchByPhone.setBounds(330, 111, 153, 23);
		customerCard.add(btnSearchByPhone);
		
		JButton btnSearchAll = new JButton("Search all");
		btnSearchAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ResultSet rs;
				try {
					rs = CustomerDao.searchAllCustomers(db);
					table.setModel(buildTableModel(rs));
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnSearchAll.setBounds(332, 135, 151, 29);
		customerCard.add(btnSearchAll);
		
		JPanel orderCard = new JPanel();
		panel.add(orderCard, "orderCard");
		orderCard.setLayout(null);
		
		JLabel lblCustomerId = new JLabel("Customer ID");
		lblCustomerId.setBounds(6, 6, 100, 16);
		orderCard.add(lblCustomerId);
		
		JLabel lblOrderDescription = new JLabel("Order Description");
		lblOrderDescription.setBounds(6, 32, 130, 16);
		orderCard.add(lblOrderDescription);
		
		JLabel lblOrderPrice = new JLabel("Order Price");
		lblOrderPrice.setBounds(6, 60, 100, 16);
		orderCard.add(lblOrderPrice);
		
		customerOrderIDField = new JTextField();
		customerOrderIDField.setBounds(133, 1, 150, 26);
		orderCard.add(customerOrderIDField);
		customerOrderIDField.setColumns(10);
		
		orderDescriptionField = new JTextField();
		orderDescriptionField.setBounds(133, 27, 150, 26);
		orderCard.add(orderDescriptionField);
		orderDescriptionField.setColumns(10);
		
		orderPriceField = new JTextField();
		orderPriceField.setBounds(133, 55, 150, 26);
		orderCard.add(orderPriceField);
		orderPriceField.setColumns(10);
		
		JButton btnPlaceOrder = new JButton("Place Order");
		btnPlaceOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String customerID = customerOrderIDField.getText();
				String orderDescription = orderDescriptionField.getText();
				String orderPrice = orderPriceField.getText();
				Order order = new Order();
				order.setCustomerId(customerID);
				order.setDescription(orderDescription);
				order.setPrice(Double.parseDouble(orderPrice));
				try {
					OrderDao.generateOrder(db, order);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (WrongCustomerIDFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IllegalOrderID e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IllegalOrderItemLength e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IllegalOrderPrice e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnPlaceOrder.setBounds(6, 104, 108, 29);
		orderCard.add(btnPlaceOrder);
		
		JButton btnGenerateDockets = new JButton("Generate dockets");
		btnGenerateDockets.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Docket.generate(db);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnGenerateDockets.setBounds(6, 132, 153, 29);
		orderCard.add(btnGenerateDockets);
		
		JLabel lblCustomerFirstName_1 = new JLabel("Customer first name");
		lblCustomerFirstName_1.setBounds(295, 6, 142, 16);
		orderCard.add(lblCustomerFirstName_1);
		
		customerOrderFirstNameField = new JTextField();
		customerOrderFirstNameField.setBounds(449, 1, 130, 26);
		orderCard.add(customerOrderFirstNameField);
		customerOrderFirstNameField.setColumns(10);
		
		JLabel lblCustomerLastName_1 = new JLabel("Customer last name");
		lblCustomerLastName_1.setBounds(295, 32, 126, 16);
		orderCard.add(lblCustomerLastName_1);
		
		JLabel lblOrderId = new JLabel("Order ID");
		lblOrderId.setBounds(295, 60, 130, 16);
		orderCard.add(lblOrderId);
		
		customerOrderLastNameField = new JTextField();
		customerOrderLastNameField.setBounds(449, 27, 130, 26);
		orderCard.add(customerOrderLastNameField);
		customerOrderLastNameField.setColumns(10);
		
		orderIDField = new JTextField();
		orderIDField.setBounds(449, 55, 130, 26);
		orderCard.add(orderIDField);
		orderIDField.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Order date");
		lblNewLabel_1.setBounds(295, 83, 126, 16);
		orderCard.add(lblNewLabel_1);
		
		orderDateField = new JTextField();
		orderDateField.setToolTipText("YY-MM-DD");
		orderDateField.setBounds(449, 81, 130, 26);
		orderCard.add(orderDateField);
		orderDateField.setColumns(10);
		
		orderTable = new JTable();
		orderTable.setBounds(6, 173, 592, 169);
		orderCard.add(orderTable);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(225, 104, 373, 36);
		orderCard.add(panel_2);
		panel_2.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblSearch = new JLabel("Search");
		panel_2.add(lblSearch);
		
		JButton btnNewButton_3 = new JButton("By name");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String firstName = customerOrderFirstNameField.getText();
				try {
					ResultSet rs = OrderDao.searchOrderByFirstName(firstName, db);
					orderTable.setModel(buildTableModel(rs));
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		panel_2.add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("By order ID");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String orderID = orderIDField.getText();
				try {
					ResultSet rs = OrderDao.searchOrderByOrderID(db, orderID);
					orderTable.setModel(buildTableModel(rs));
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		panel_2.add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("By date");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String date = orderDateField.getText();
				try {
					ResultSet rs = OrderDao.searchOrderByDate(date, db);
					orderTable.setModel(buildTableModel(rs));
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		panel_2.add(btnNewButton_5);
		
		JPanel invoiceCard = new JPanel();
		panel.add(invoiceCard, "invoiceCard");
		invoiceCard.setLayout(null);
		
		invoiceTable = new JTable();
		invoiceTable.setBounds(6, 159, 592, 183);
		invoiceCard.add(invoiceTable);
		
		
		
		JLabel lblNewLabel_2 = new JLabel("Invoice ID");
		lblNewLabel_2.setBounds(6, 6, 78, 16);
		invoiceCard.add(lblNewLabel_2);
		
		invoiceIDField = new JTextField();
		invoiceIDField.setBounds(96, 1, 130, 26);
		invoiceCard.add(invoiceIDField);
		invoiceIDField.setColumns(10);
		
		JLabel lblInvoiceDate = new JLabel("Invoice Date");
		lblInvoiceDate.setBounds(6, 34, 78, 16);
		invoiceCard.add(lblInvoiceDate);
		
		invoiceDateField = new JTextField();
		invoiceDateField.setBounds(96, 29, 130, 26);
		invoiceCard.add(invoiceDateField);
		invoiceDateField.setColumns(10);
		
		JLabel lblOrderId_1 = new JLabel("Order ID");
		lblOrderId_1.setBounds(6, 62, 78, 16);
		invoiceCard.add(lblOrderId_1);
		
		JLabel lblPaymentMethod = new JLabel("Payment method");
		lblPaymentMethod.setBounds(6, 90, 113, 16);
		invoiceCard.add(lblPaymentMethod);
		
		invoiceOrderIDField = new JTextField();
		invoiceOrderIDField.setBounds(96, 57, 130, 26);
		invoiceCard.add(invoiceOrderIDField);
		invoiceOrderIDField.setColumns(10);
		
		JComboBox<String> paymentMethodBox = new JComboBox<String>();
		paymentMethodBox.setModel(new DefaultComboBoxModel<String>(new String[] {"Cash", "Cheque"}));
		paymentMethodBox.setBounds(117, 86, 99, 27);
		invoiceCard.add(paymentMethodBox);
		
		JLabel label = new JLabel("Customer first name");
		label.setBounds(241, 6, 142, 16);
		invoiceCard.add(label);
		
		invoiceFirstNameField = new JTextField();
		invoiceFirstNameField.setColumns(10);
		invoiceFirstNameField.setBounds(395, 1, 130, 26);
		invoiceCard.add(invoiceFirstNameField);
		
		JLabel label_1 = new JLabel("Customer last name");
		label_1.setBounds(241, 32, 126, 16);
		invoiceCard.add(label_1);
		
		JLabel label_2 = new JLabel("Order ID");
		label_2.setBounds(241, 60, 130, 16);
		invoiceCard.add(label_2);
		
		invoiceLastNameField = new JTextField();
		invoiceLastNameField.setColumns(10);
		invoiceLastNameField.setBounds(395, 27, 130, 26);
		invoiceCard.add(invoiceLastNameField);
		
		invoiceOrderIDSearchField = new JTextField();
		invoiceOrderIDSearchField.setColumns(10);
		invoiceOrderIDSearchField.setBounds(395, 55, 130, 26);
		invoiceCard.add(invoiceOrderIDSearchField);
		
		JLabel lblInvoiceDate_1 = new JLabel("Invoice Date");
		lblInvoiceDate_1.setBounds(241, 83, 126, 16);
		invoiceCard.add(lblInvoiceDate_1);
		
		invoiceDateSearchField = new JTextField();
		invoiceDateSearchField.setBounds(395, 80, 130, 26);
		invoiceCard.add(invoiceDateSearchField);
		invoiceDateSearchField.setColumns(10);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBounds(214, 111, 384, 36);
		invoiceCard.add(panel_3);
		panel_3.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel label_3 = new JLabel("Search");
		panel_3.add(label_3);
		
		JButton button = new JButton("By first name");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String firstName = invoiceFirstNameField.getText();
				try {
					ResultSet rs = InvoiceDao.searchInvoiceByCustomerFirstName(firstName, db);
					invoiceTable.setModel(buildTableModel(rs));
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		panel_3.add(button);
		
		JButton button2 = new JButton("By last name");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String lastName = invoiceLastNameField.getText();
				try {
					ResultSet rs = InvoiceDao.searchInvoiceByCustomerLastName(lastName, db);
					invoiceTable.setModel(buildTableModel(rs));
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		panel_3.add(button2);
		
		JButton button_1 = new JButton("By order ID");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String orderID = invoiceOrderIDSearchField.getText();
				try {
					ResultSet rs = InvoiceDao.searchInvoiceByOrderID(orderID, db);
					invoiceTable.setModel(buildTableModel(rs));
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (HeadlessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IllegalOrderID e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		panel_3.add(button_1);
		
		JButton button_2 = new JButton("By date");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String date = invoiceDateSearchField.getText();
				try {
					ResultSet rs = InvoiceDao.searchInvoiceByDate(date, db);
					invoiceTable.setModel(buildTableModel(rs));
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		panel_3.add(button_2);
		
		JButton btnGenerateInvoice = new JButton("Generate Invoice");
		btnGenerateInvoice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String invoiceID = invoiceIDField.getText();
				String invoiceDate = invoiceDateField.getText();
				String invoiceOrderID = invoiceOrderIDField.getText();
				String paymentMethod = (String) paymentMethodBox.getSelectedItem();
				Invoice invoice = new Invoice();
				invoice.setInvoiceID(invoiceID);
				invoice.setInvoiceDate(invoiceDate);
				invoice.setOrderID(invoiceOrderID);
				invoice.setPaymentMethod(paymentMethod);
				try {
					InvoiceDao.generateInvoice(db, invoice);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnGenerateInvoice.setBounds(6, 115, 137, 29);
		invoiceCard.add(btnGenerateInvoice);
		
		
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(10, 11, 604, 34);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JButton btnNewButton = new JButton("Customers");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CardLayout cardLayout = (CardLayout) panel.getLayout();
				cardLayout.show(panel, "customerCard");
			}
		});
		panel_1.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Orders");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CardLayout cardLayout = (CardLayout) panel.getLayout();
				cardLayout.show(panel, "orderCard");
			}
		});
		panel_1.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Invoices");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CardLayout cardLayout = (CardLayout) panel.getLayout();
				cardLayout.show(panel, "invoiceCard");
			}
		});
		panel_1.add(btnNewButton_2);
		frame.setVisible(true);
		centreWindow(frame);

		frame.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				db.closeAll();
			}
		});
	}
	
	public static void centreWindow(Window frame) {
	    Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
	    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
	    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
	    frame.setLocation(x, y);
	}
}
