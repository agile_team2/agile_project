package tests;

import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import javax.print.PrintService;
import core.PrintDocket;
import exceptions.NoFileExistException;
import exceptions.NoPrintServiceFoundException;
import junit.framework.TestCase;

public class PrintDocketTest extends TestCase {
	PrintDocket pd = new PrintDocket();

	/***
	 * Test No.1
	 * Objective: To test the print if file is present
	 * Inputs: Correct file Name
	 * Expected Output: The function should return true if the file is printed successfully.
	 
	 ***/
	public void testForSuccessfull() throws  NoFileExistException, PrinterException {
		assertTrue(PrintDocket.print("docket_O387605.txt"));
	}
	

	/***
	 * Test No.2
	 * Objective: To test the print to throw exception if file is not present.
	 * Inputs: InCorrect file Name
	 * Expected Output: The function should throw an Exception of No File Found.
	 
	 ***/
	
	 public void testForNotFileFind() {
		 try {
				PrintDocket.print("test");
				fail("Exception expected");
			} catch (  NoFileExistException | PrinterException e) {
				assertEquals(e.getMessage(), "No File Found");
			}
	 }

		/***
		 * Test No.3
		 * Objective: To test the print throw exception if no print services are present.
		 * Inputs: Correct file Name
		 * Expected Output: The function should throw an Exception of No service Found.
		 
		 ***/
	 
	 public void testForNoServiceFind() {
		 try {
				PrintDocket.print("docket_O387605.txt");
				PrinterJob job = PrinterJob.getPrinterJob();
				 PrintService services = job.getPrintService();
				 if(services.toString().length() == 0) {
					 fail("Exception expected");
				}
				
			}
		 catch (  NoFileExistException | PrinterException e) {
				assertEquals("No printers available", e.getMessage());		}
	 }


}
