package tests;

import java.sql.ResultSet;
import java.sql.SQLException;

import core.OrderDao;
import db.DBUtil;
import exceptions.IllegalOrderItemLength;
import exceptions.IllegalOrderPrice;
import junit.framework.TestCase;

public class EditOrderTesting extends TestCase {
	private static DBUtil dbUtil;
	
	protected void setUp() throws Exception {
		super.setUp();
		dbUtil = DBUtil.getDBUtilInstance();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		dbUtil.closeAll();
	}
	
	/***
	 * Test No.1
	 * Objective: To test if the order is edited on the description and price
	 * Inputs: order_description = "Weekly newspapers", price = 2.0, order_id = "O041474"
	 * Expected Output: The number of affected record is 1, and the changed the record  can be selected.
	 ***/
	public void testEditOrderDecriptionPrice001() throws SQLException, IllegalOrderPrice, IllegalOrderItemLength {
		assertEquals(OrderDao.editOrderDecriptionPrice("O129580", "Weekly newspaper", 2.0, dbUtil), 1);
		String sql = "select * from Orders where order_id = 'O129580' and order_description = 'Weekly newspaper' and order_price = '2.0'";
		ResultSet resultSet =  dbUtil.executeQuery1(sql);
		resultSet.last();
		assertEquals(resultSet.getRow(),1);
	}
	
	/***
	 * Test No.2
	 * Objective: To test if the system will be crashed when the order id is not existed.
	 * Inputs: order_description = "Weekly newspapers", price = 2.0, order_id = "O290691"
	 * Expected Output: The number of affected record is 0
	 ***/
	public void testEditOrderDecriptionPrice002() throws SQLException, IllegalOrderPrice, IllegalOrderItemLength{
		assertEquals(OrderDao.editOrderDecriptionPrice("O290691", "Weekly newspaper", 2.0, dbUtil), 0);
	}
	
	/***
	 * Test No.3
	 * Objective: To test if the order is edited on the description
	 * Inputs: order_description = "The Independent", order_id = "O129580"
	 * Expected Output: The number of affected record is 1, and the changed the record  can be selected.
	 ***/
	public void testEditOrderDecription001() throws SQLException, IllegalOrderItemLength{
		assertEquals(OrderDao.editOrderDecription("O129580", "The Independent", dbUtil), 1);
		String sql = "select * from Orders where order_id = 'O129580' and order_description = 'The Independent'";
		ResultSet resultSet =  dbUtil.executeQuery1(sql);
		resultSet.last();
		assertEquals(resultSet.getRow(),1);
	}
	
	/***
	 * Test No.4
	 * Objective: To test if the system will be crashed when the order id is not existed.
	 * Inputs: order_description = "The Independent", order_id = "O290691"
	 * Expected Output: The number of affected record is 0
	 ***/
	public void testEditOrderDecription002() throws SQLException, IllegalOrderItemLength{
		assertEquals(OrderDao.editOrderDecription("O290691", "Weekly newspapers", dbUtil), 0);
	}
	
	/***
	 * Test No.5
	 * Objective: To test if the order is edited on the price
	 * Inputs: price = 2.0, order_id = "O129580"
	 * Expected Output: The number of affected record is 1,and the changed the record  can be selected. 
	 ***/
	public void testEditOrderPrice001() throws SQLException, IllegalOrderPrice{
		assertEquals(OrderDao.editOrderPrice("O129580",1.0, dbUtil), 1);
		String sql = "select * from Orders where order_id = 'O129580' and order_price = '1.0'";
		ResultSet resultSet =  dbUtil.executeQuery1(sql);
		resultSet.last();
		assertEquals(resultSet.getRow(),1);
	}
	
	/***
	 * Test No.6
	 * Objective: To test if the system will be crashed when the order id is not existed.
	 * Inputs: price = 2.0, order_id = "O290691"
	 * Expected Output: The number of affected record is 0
	 ***/
	public void testEditOrderPrice002() throws SQLException, IllegalOrderPrice{
		assertEquals(OrderDao.editOrderPrice("O290691",2.0, dbUtil), 0);
	}
	
	/***
	 * Test No.7
	 * Objective: To test if the order is edited on the isActive
	 * Inputs: order_id = "O129580"
	 * Expected Output: The number of affected record is 1,and the changed the record  can be selected.
	 ***/
	public void testSetOrderActive001() throws SQLException {
		assertEquals(OrderDao.setOrderActive("O129580", dbUtil), 1);
		String sql = "select * from Orders where order_id = 'O129580' and isActive = 'Yes'";
		ResultSet resultSet =  dbUtil.executeQuery1(sql);
		resultSet.last();
		assertEquals(resultSet.getRow(),1);
	}
	
	/***
	 * Test No.8
	 * Objective: To test if the system will be crashed when the order id is not existed.
	 * Inputs:order_id = "O290691"
	 * Expected Output: The number of affected record is 0
	 ***/
	public void testSetOrderActive002() throws SQLException{
		assertEquals(OrderDao.setOrderActive("O290691", dbUtil), 0);
	}
	
	/***
	 * Test No.9
	 * Objective: To test if the order is edited on the isActive
	 * Inputs: order_id = "O387605"
	 * Expected Output: The number of affected record is 1,and the changed the record  can be selected.
	 ***/
	public void testSetOrderInactive001() throws SQLException{
		assertEquals(OrderDao.setOrderInactive("O387605", dbUtil), 1);
		String sql = "select * from Orders where order_id = 'O387605' and isActive is null";
		ResultSet resultSet =  dbUtil.executeQuery1(sql);
		resultSet.last();
		assertEquals(resultSet.getRow(),1);
	}
	
	/***
	 * Test No.10
	 * Objective: To test if the system will be crashed when the order id is not existed.
	 * Inputs:order_id = "O290691"
	 * Expected Output: The number of affected record is 0
	 ***/
	public void testSetOrderInactive002() throws SQLException {
		assertEquals(OrderDao.setOrderInactive("O290691", dbUtil), 0);
	}
	
}
