package tests;



import java.sql.SQLException;

import core.Account;
import db.DBUtil;
import exceptions.EmptyFieldException;
import junit.framework.TestCase;

public class AccountTest extends TestCase {

	private static Account ac = new Account();
	private static DBUtil db;
	
	/***
	 * Test No.1
	 * Objective: To test the login method
	 * Inputs: set correct username and set password
	 * Expected Output: The function should return true if it run successfully.
	 
	 ***/
	public void testLogin() throws Exception{
		ac.setUsername("admin");
		ac.setPassword("admin");
		db =  DBUtil.getDBUtilInstance();
		assertTrue(Account.login(db, "admin", "admin"));
	}
	
	/***
	 * Test No.2
	 * Objective: To test the login method
	 * Inputs: set incorrect username and set password
	 * Expected Output: The function should return false if it run successfully.
	 
	 ***/
	public void testWrongInput() throws Exception {
		
		ac.setUsername("Admin");
		ac.setPassword("Admin");
		db =  DBUtil.getDBUtilInstance();
		assertFalse(Account.login(db, "Admin", "Admin"));
	}
	
	/***
	 * Test No.3
	 * Objective: To test the validate method
	 * Inputs: set username and set password as empty
	 * Expected Output: The function should throw an exception.
	 
	 ***/
	public void testValidateUsernamePassword() throws  Exception {
		ac.setUsername("");
		ac.setPassword("");
		
		try {
			
			ac.validateUsernamePassword();
		
		}
		catch (EmptyFieldException e) {
			// TODO Auto-generated catch block
			assertEquals(e.getMessage(),"Empty field!");
		}
	}
	
	/***
	 * Test No.4
	 * Objective: To test the validate method
	 * Inputs: set username and set password.
	 * Expected Output: The function should not throw an exception.
	 
	 ***/
	public void testValidateUsernamePassword2() throws Exception {
		ac.setUsername("admin");
		ac.setPassword("admin");
		
		assertTrue(ac.validateUsernamePassword());
	}

}
