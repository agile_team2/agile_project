package tests;

import db.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;

import core.CustomerDao;
import junit.framework.TestCase;

public class CustomerSearchTest extends TestCase {
	
	private static DBUtil db;

	protected void setUp() throws Exception {
		super.setUp();
		db = DBUtil.getDBUtilInstance();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		db.closeAll();
	}
	
	/*
	 * Test no.1
	 * Objective: To search all customers
	 * Inputs: -
	 * Expected Output: True
	 */
	public void testSearchAllCustomers() throws SQLException {
		ResultSet rs = CustomerDao.searchAllCustomers(db);
		assertTrue(rs.next());
	}
	
	/*
	 * Test no. 2
	 * Objective: To search a customer by first name that does not exist in database
	 * Inputs: String "Dwayne"
	 * Expected Output: False
	 */
	public void testSearchCustomerByFirstName001() throws SQLException {
		ResultSet rs = CustomerDao.searchByFirstName(db, "Dwayne");
		assertFalse(rs.next());
	}
	
	/*
	 * Test no. 2
	 * Objective: To search a customer by first name that does exist in database
	 * Inputs: String "John"
	 * Expected Output: True
	 */
	public void testSearchCustomerByFirstName002() throws SQLException {
		ResultSet rs = CustomerDao.searchByFirstName(db, "John");
		assertTrue(rs.next());
	}
	
	/*
	 * Test no. 3
	 * Objective: To search a customer by last name that does not exist in database
	 * Inputs: String "Johnson"
	 * Expected Output: False
	 */
	public void testSearchCustomerByLastName001() throws SQLException {
		ResultSet rs = CustomerDao.searchByLastName(db, "Johnson");
		assertFalse(rs.next());
	}
	
	/*
	 * Test no. 4
	 * Objective: To search a customer by last name that does exist in database
	 * Inputs: String "Doe"
	 * Expected Output: True
	 */
	public void testSearchCustomerByLastName002() throws SQLException {
		ResultSet rs = CustomerDao.searchByLastName(db, "Doe");
		assertTrue(rs.next());
	}
	
	/*
	 * Test no. 5
	 * Objective: To search a customer by eircode that does not exist in database
	 * Inputs: String "y213312e"
	 * Expected Output: False
	 */
	public void testSearchCustomerByEircode001() throws SQLException {
		ResultSet rs = CustomerDao.searchByEircode(db, "y213312e");
		assertFalse(rs.next());
	}
	
	/*
	 * Test no. 6
	 * Objective: To search a customer by eircode that does exist in database
	 * Inputs: String "N67 T4K7"
	 * Expected Output: True
	 */
	public void testSearchCustomerByEircode002() throws SQLException {
		ResultSet rs = CustomerDao.searchByEircode(db, "N67 T4K7");
		assertTrue(rs.next());
	}
	
	/*
	 * Test no. 7
	 * Objective: To search a customer by phone number that does not exist in database
	 * Inputs: String "092929"
	 * Expected Output: False
	 */
	public void testSearchCustomerByPhoneNumber001() throws SQLException {
		ResultSet rs = CustomerDao.searchByPhoneNumber(db, "092929");
		assertFalse(rs.next());
	}
	
	/*
	 * Test no. 8
	 * Objective: To search a customer by phone number that does exist in database
	 * Inputs: String "0879464637"
	 * Expected Output: True
	 */
	public void testSearchCustomerByPhoneNumber002() throws SQLException {
		ResultSet rs = CustomerDao.searchByPhoneNumber(db, "0879464637");
		assertTrue(rs.next());
	}
	
	/*
	 * Test no. 9
	 * Objective: To search a customer by delivery area that does not exist in database
	 * Inputs: String "Ranelagh"
	 * Expected Output: false
	 */
	public void testSearchCustomerByDeliveryArea001() throws SQLException {
		ResultSet rs = CustomerDao.searchByDeliveryArea(db, "Ranelagh");
		assertFalse(rs.next());
	}
	
	/*
	 * Test no. 10
	 * Objective: To search a customer by delivery area that does exist in database
	 * Inputs: String "Glasson"
	 * Expected Output: true 
	 */
	public void testSearchCustomerByDeliveryArea002() throws SQLException {
		ResultSet rs = CustomerDao.searchByDeliveryArea(db, "Glasson");
		assertTrue(rs.next());
	}
	
	/*
	 * Test no. 11
	 * Objective: To search for all inactive customers
	 * Inputs: -
	 * Expected Output: true
	 */
	public void testSearchAllInactiveCustomers001() throws SQLException {
		ResultSet rs = CustomerDao.searchAllInactiveCustomers(db);
		assertTrue(rs.next());
	}
	
	/*
	 * Test no. 12
	 * Objective: To search for all delivery areas
	 * Inputs: -
	 * Expected Output: true
	 */
	public void testSearchAllDeliveryAreas001() throws SQLException {
		ResultSet rs = CustomerDao.getAllDeliveryAreas(db);
		assertTrue(rs.next());
	}
	
	/*
	 * Test no. 12
	 * Objective: To search by id which does not exist
	 * Inputs: String "C123213123"
	 * Expected Output: false
	 */
	public void testSearchCustomerById001() throws SQLException {
		ResultSet rs = CustomerDao.searchById(db, "C123213123");
		assertFalse(rs.next());
	}
	
	/*
	 * Test no. 13
	 * Objective: To search by id which does exist
	 * Inputs: String "C123456"
	 * Expected Output: true
	 */
	public void testSearchCustomerById002() throws SQLException {
		ResultSet rs = CustomerDao.searchById(db, "C123456");
		assertTrue(rs.next());
	}
}
