package tests;

import junit.framework.TestCase;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.sql.ResultSet;
import java.sql.SQLException;

import core.Invoice;
import core.InvoiceDao;
import db.DBUtil;
import exceptions.IllegalOrderID;
import exceptions.InvalidInvoiceIDException;
import exceptions.InvalidPaymentMethodException;

public class InvoiceTests extends TestCase {
	private static Invoice invoice;
	private static DBUtil dbUtil;
	private static ResultSet resultSet;
	protected void setUp() throws Exception {
		super.setUp();
		invoice = new Invoice();
		resultSet = null;
		dbUtil = DBUtil.getDBUtilInstance();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		dbUtil.closeAll();
	}
	
	/*
	 Test Number: 1
	 Test Type: JUnit
	 Test Objective: Enter a valid Payment Method
	 Inputs: cash
	 Expected Output: validatePaymentMethod will return true
	 */

	public void testValidatePaymentMethod001() {
		Invoice invoice = new Invoice();
		invoice.setPaymentMethod("Cash");
		try {
			assertTrue(invoice.validatePaymentMethod());
		} catch (InvalidPaymentMethodException e) {

		}
	}

	/*
	 Test Number: 2
	 Test Type: JUnit
	 Test Objective: Enter a valid Payment Method
	 Inputs: cheque
	 Expected Output: validatePaymentMethod will return true
	 */

	public void testValidatePaymentMethod002() {
		Invoice invoice = new Invoice();
		invoice.setPaymentMethod("Cheque");
		try {
			assertTrue(invoice.validatePaymentMethod());
		} catch (InvalidPaymentMethodException e) {

		}
	}

	/*
	 Test Number: 3
	 Test Type: JUnit
	 Test Objective: Enter an invalid Payment Method
	 Inputs: card 
	 Expected Output: validatePaymentMethod will return false
	 */
	public void testValidatePaymentMethod003() {
		Invoice invoice = new Invoice();
		invoice.setPaymentMethod("card");
		try {
			assertTrue(invoice.validatePaymentMethod());
			fail();
		} catch (InvalidPaymentMethodException e) {
			assertEquals(e.getMessage(), "This invoice payment method is invalid.");
		}
	}

	/*
	 Test Number: 4 
	 Test Type: JUnit
	 Test Objective: Generate a valid Invoice
	 Inputs: invoiceID = I123456 PaymentMethod = cash InvoiceDate= 19/02/28
	 isOverdue = null isPaidFor = Yes OrderID = 1234567891234 
	 Expected Output: generateInvoice method will return true
	 */

	public void testGenerateInvoice001() throws SQLException {
		Invoice invoice = new Invoice();
		invoice.setPaymentMethod("cash");
		invoice.setInvoiceDate("19/02/28");
		invoice.setIsOverdue(null);
		invoice.setIsPaidFor("Yes");
		invoice.setOrderID("O129580");
		assertEquals(InvoiceDao.generateInvoice(dbUtil, invoice), true);
	}
	
	/*
	 Test Number: 5
	 Test Type: JUnit
	 Test Objective: Input a valid invoice id
	 Inputs: I123456
	 Expected Outputs: The validateInvoiceID method will return true
	 */

	public void testvalidateInvoiceID001() throws Exception {
		invoice.setInvoiceID("I123456");
		assertTrue(invoice.validateInvoiceID());
	}

	/*
	 Test Number: 6
	 Test Type: JUnit
	 Test Objective: Input an invalid invoice id
	 Inputs: Z1234567
	 Expected Outputs: The validateInvoiceID method will print exception message
	 */

	public void testvalidateInvoiceID002() {
		invoice.setInvoiceID("300");
		try {
			assertTrue(invoice.validateInvoiceID());
			fail();
		} catch (InvalidInvoiceIDException e) {
			assertEquals(e.getMessage(), "This invoice ID is invalid.");
		}
	}
	
	/*
	 Test Number: 7
	 Test Type: JUnit
	 Test Objective: set invoice overdue
	 Inputs: I123490
	 Expected Outputs: The method will return true
	 */
	public void testsetInvoiceOverdue001()throws SQLException, IllegalOrderID
	{
		assertEquals(InvoiceDao.setInvoiceOverdue("I123490", dbUtil), 1);
	}
	
	/*
	 Test Number: 8
	 Test Type: JUnit
	 Test Objective: Selects all invoices
	 Inputs: The SQL query = "Select * From Invoices;"
	 Expected Output: returns the output of the SQL query
	 */
	public void testsearchAllInvoices001() throws SQLException
	{
		assertNotNull(InvoiceDao.searchAllInvoices(dbUtil));
	}
	
	/*
	 Test Number: 9
	 Test Type: JUnit
	 Test Objective: Set Invoice as paid for
	 Inputs: I167845
	 Expected Outputs: The method will return true
	 */
	
	public void testsetInvoicePaidFor001() throws SQLException
	{
		assertEquals(InvoiceDao.setInvoicePaidFor("I167845", dbUtil), 1);
	}
}
