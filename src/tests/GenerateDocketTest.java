package tests;

import core.Docket;

import db.DBUtil;
import junit.framework.TestCase;

public class GenerateDocketTest extends TestCase{
	private static DBUtil dBUtil;

	
	/***
	 * Test No.1
	 * Objective: To test the GenerateByArea method
	 * Inputs: Data base object and Correct area code
	 * Expected Output: The function should run successfully and return true.
	 
	 ***/
	
	public void test1() throws Exception {
		dBUtil = DBUtil.getDBUtilInstance();
		assertTrue(Docket.generateByArea(dBUtil, "Willow Park"));
	}
	
	/***
	 * Test No.2
	 * Objective: To test the GenerateByArea method
	 * Inputs: Data base object and Correct area.
	 * Expected Output: The function should return TRUE.
	 
	 ***/
	public void test2() throws Exception {
		dBUtil = DBUtil.getDBUtilInstance();
		assertTrue(Docket.generateByArea(dBUtil, "Irish Town"));
	}
	
	
	/***
	 * Test No.3
	 * Objective: To test the GenerateByArea method
	 * Inputs: Data base object and InCorrect area code
	 * Expected Output: The function should return false.
	 
	 ***/
	public void test3() throws Exception {
		dBUtil = DBUtil.getDBUtilInstance();
		assertFalse(Docket.generateByArea(dBUtil, "Irish T"));
	}
	
	/***
	 * Test No.4 
	 * Objective: To test the GenerateByArea method
	 * Inputs: Data base object and Correct area code but the IsActive is set to NO
	 * Expected Output: The function should return false.
	 
	 ***/
	public void test4() throws Exception {
		dBUtil = DBUtil.getDBUtilInstance();
		assertFalse(Docket.generateByArea(dBUtil, "Summerhill"));
	}
	
	/***
	 * Test No.5
	 * Objective: To test the Generate method
	 * Inputs: Data base object
	 * Expected Output: The function should return True.
	 
	 ***/
	public void test5() throws Exception {
		dBUtil = DBUtil.getDBUtilInstance();
		assertTrue(Docket.generate(dBUtil));
	}

}
