package tests;

import junit.framework.TestCase;
import java.sql.ResultSet;
import java.sql.SQLException;

import core.InvoiceDao;
import db.DBUtil;

public class GenerateInvoiceSummary extends TestCase {
	
	private static ResultSet resultSet;
	private static DBUtil dbUtil;

	protected void setUp() throws Exception {
		super.setUp();
		dbUtil = DBUtil.getDBUtilInstance();
		resultSet = null;
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		dbUtil.closeAll();
	}
	
	/*
	 Test Number: 1
	 Test Type: JUnit
	 Test Objective: To test invoices that have been paid for
	 Inputs: Nothing
	 Expected Output: The Invoices that are paid for.
	 */
	
	public void testsearchInvoicePaidFor001() throws SQLException
	{
		resultSet = InvoiceDao.searchInvoicesPaidFor(dbUtil);
		resultSet.next();
		String contentString = resultSet.getString(1);
		assertNotNull(contentString);
	}
	
	/*
	 Test Number: 2
	 Test Type: JUnit
	 Test Objective: To test if you can print out a Invoice Summary
	 Inputs: Nothing
	 Expected Outputs: The summary will be printed out into a text file
	 */
	
	public void testprintInvoiceSummary001()
	{
		try {
			InvoiceDao.printInvoiceSummary(dbUtil);
		} catch (Exception e) {
			fail();
		}
	}
	
	/*
	 Test Number: 3
	 Test Type: JUnit
	 Test Objective: To test if that you print out a Invoice Summary
	 Inputs: Nothing
	 Expected Outputs: The summary will be printed out into a text file
	 */
	
	public void testprintMonthlyInvoiceSummary001()
	{
		String monthString = "March";
		try {
			InvoiceDao.printMonthlyInvoiceSummary(dbUtil, monthString);
		} catch (Exception e) {
			fail();
		}
	}
}
