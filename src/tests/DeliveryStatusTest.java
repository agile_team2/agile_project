package tests;

import java.sql.SQLException;

import core.DeliveryStatus;
import core.Order;
import db.DBUtil;
import exceptions.IllegalOrderID;
import exceptions.IllegalOrderPrice;
import junit.framework.TestCase;

public class DeliveryStatusTest extends TestCase {
	DeliveryStatus ds = new DeliveryStatus();
	 DBUtil db = new DBUtil();
    
	
	 
	 /***
		 * Test No.1
		 * Objective: To test the UpdateIsDelivered Method to throw Exception.
		 * Inputs: Enter the Correct Order Id 
		 * Expected Output: The function should return 1 if it run successfully.
		 
		 ***/
	 
	public void testUpdateIsDelivered() throws Exception{
		
			 assertEquals(1, ds.UpdateIsDelivered(db, "O348755"));
		
	}
	

	 /***
		 * Test No.2
		 * Objective: To test the UpdateIsDelivered Method to throw Exception.
		 * Inputs: Enter the InCorrect Order Id 
		 * Expected Output: The function should return 0 if does not run successfully & throw an Exception.
	 * @throws Exception 
	 * @throws IllegalOrderID 
		 
		 ***/
	 
	public void testUpdateIsDelivered2() throws Exception  {
		
			 try {
				assertEquals(0,ds.UpdateIsDelivered(db, "02"));
			} catch (IllegalOrderID e) {
				// TODO Auto-generated catch block
				
			} 
		
		
		
	}

	 /***
		 * Test No.3
		 * Objective: To test the Validate Method.
		 * Inputs: Enter the Correct Order Id 
		 * Expected Output: The function should run without any Exception.
		 
		 ***/
	 
	
	public void testOrderNumber() throws IllegalOrderID{
      assertTrue(ds.validateOrderNumber("O270611"));
			
		

	}


	 /***
		 * Test No.4
		 * Objective: To test the validate method through UpdateIsDelivered method .
		 * Inputs: Enter the InCorrect Order Id 
		 * Expected Output: The function should  throw an Exception.
		 
		 ***/
	 
	public void testorder2() throws Exception{
		try {
			 ds.UpdateIsDelivered(db, "02");
		} 
		catch (IllegalOrderID e) {
			// TODO Auto-generated catch block
			assertEquals("This order ID is invalid!", e.getMessage());
		}
		
	}
	

}
