package tests;

import java.awt.HeadlessException;
import java.sql.ResultSet;
import java.sql.SQLException;

import core.OrderDao;
import db.DBUtil;
import exceptions.IllegalOrderID;
import junit.framework.TestCase;

public class SearchOrderTesting extends TestCase {

	private static ResultSet resultSet;
	private static DBUtil dbUtil;

	protected void setUp() throws Exception {
		super.setUp();
		dbUtil = DBUtil.getDBUtilInstance();
		resultSet = null;
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		dbUtil.closeAll();
	}

	/***
	 * Test No.1 Objective: To test the order can be selected from database table.
	 * Inputs: orderID = "O827979", this ID exists in database. Expected
	 * Output: a not null string
	 */
	public void testSearchOrderByOrderID001() throws SQLException {
		String content = null;
		resultSet = OrderDao.searchOrderByOrderID(dbUtil, "O129580");
		resultSet.next();
		content = resultSet.getString(1);
		assertNotNull(content);
	}

	/***
	 * Test No.2 Objective: To test the order cannot be selected from database
	 * table. Inputs: orderID = "O123451", this ID dose not exist in
	 * database. Expected Output: a null string, so throw database exception
	 */
	public void testSearchOrderByOrderID002() throws SQLException {
		resultSet = OrderDao.searchOrderByOrderID(dbUtil, "O123451");
		try {
			resultSet.next();
			resultSet.getString(1);
			fail();
		} catch (SQLException e) {

		}
	}

	/***
	 * Test No.3 Objective: To test the order can be selected from database table.
	 * Inputs: firstName = "Michael",lastName = "Chen", name exists in database.
	 * Expected Output: a not null String object
	 */
	public void testSearchOrderByName001() throws SQLException {
		resultSet = OrderDao.searchOrderByName("Bill", "Gates", dbUtil);
		String content = null;
		resultSet.next();
		content = resultSet.getString(1);
		assertNotNull(content);
	}

	/***
	 * Test No.4 Objective: To test the order cannot be selected from database
	 * table. Inputs: firstName = "Jim",lastName = "Chen", the name dose not exist
	 * in database. Expected Output: a null String object, so throw database
	 * exception
	 */
	public void testSearchOrderByName002()
			throws SQLException {
		resultSet = OrderDao.searchOrderByName("Jim", "Chen", dbUtil);
		try {
			resultSet.next();
			resultSet.getString(1);
		} catch (SQLException e) {

		}

	}

	/***
	 * Test No.5 Objective: To test the order can be selected from database table.
	 * Inputs: date = "2019/03/16" 
	 * Expected Output: a not null String object
	 */
	public void testSearchOrderByDate001()throws SQLException {
		String dateString = "2019/03/16";
		resultSet = OrderDao.searchOrderByDate(dateString, dbUtil);
		String content = null;
		resultSet.next();
		content = resultSet.getString(1);
		assertNotNull(content);
	}

	/***
	 * Test No.6 Objective: To test the order cannot be selected from database
	 * table. Inputs: date = "2019/02/22" 
	 * Expected Output: a null String object, so
	 * throw database exception
	 */
	public void testSearchOrderByDate002()
			throws SQLException {
		String dateString = "2019/02/22";
		resultSet = OrderDao.searchOrderByDate(dateString, dbUtil);
		try {
			resultSet.next();
			resultSet.getString(1);
			fail();
		} catch (SQLException e) {

		}
	}
	/***
	 * Test No.7 Objective: To test the order can be selected from database
	 * table. Inputs: firstName = "Jim", the name exists.
	 * in database. 
	 * Expected Output: a not null String object
	 * exception
	 */
	public void testSearchOrderByFirstName001()throws SQLException {
		resultSet = OrderDao.searchOrderByFirstName("Jim", dbUtil);
		String content = null;
		resultSet.next();
		content = resultSet.getString(1);
		assertNotNull(content);
	}

	/***
	 * Test No.8 Objective: To test the order cannot be selected from database
	 * table. Inputs: firstName = "Steven", the name dose not exist
	 * in database. 
	 * Expected Output: a null String object, so throw database
	 * exception
	 */
	public void testSearchOrderByFirstName002()throws SQLException {
		resultSet = OrderDao.searchOrderByFirstName("Steven", dbUtil);
		try {
			resultSet.next();
			resultSet.getString(1);
		} catch (SQLException e) {

		}

	}
	
	/***
	 * Test No.9 Objective: To test the order can be selected from database
	 * table. Inputs: lastName = "Sharp", the name exists.
	 * in database. 
	 * Expected Output:  a not null String object
	 * exception
	 */
	public void testSearchOrderByLastName001()throws SQLException {
		resultSet = OrderDao.searchOrderByLastName("Sharp", dbUtil);
		String content = null;
		resultSet.next();
		content = resultSet.getString(1);
		assertNotNull(content);
	}

	/***
	 * Test No.8 Objective: To test the order cannot be selected from database
	 * table. Inputs: lastName = "Chen", the name dose not exist
	 * in database. 
	 * Expected Output: a null String object, so throw database
	 * exception
	 */
	public void testSearchOrderByLastName002()throws SQLException {
		resultSet = OrderDao.searchOrderByFirstName("Chen", dbUtil);
		try {
			resultSet.next();
			resultSet.getString(1);
		} catch (SQLException e) {

		}

	}
	
	/***
	 * Test No.10 Objective: To test all orders can be selected from database
	 * table. Inputs: no parameters but a static sql query
	 * in database. 
	 * Expected Output:  a not null String object
	 * exception
	 */
	public void testSearchAllOrders001()throws SQLException {
		resultSet = OrderDao.searchAllOrders(dbUtil);
		String content = null;
		resultSet.next();
		content = resultSet.getString(1);
		assertNotNull(content);
	}
}
