package tests;

import junit.framework.TestCase;

import java.sql.SQLException;

import core.Customer;
import core.CustomerDao;
import db.DBUtil;
import exceptions.CustomerException;

public class CustomerTest extends TestCase {
	
	private static DBUtil db;
	
	protected void setUp() throws Exception {
		super.setUp();
		db = DBUtil.getDBUtilInstance();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		db.closeAll();
	}
	
	/*
	 * Test no.1
	 * Objective: To test name in range 0 to 1 characters
	 * Inputs: firstName = "A"
	 * Expected Output: Exception message: "First name cannot be less than 2 characters"
	 */
	public void testValidateFirstName001() {
		try {
			Customer.validateName("A");
			fail("Exception expected");
		} catch (CustomerException e) {
			assertEquals("Name cannot be less than 2 characters", e.getMessage());
		}
	}
	
	/*
	 * Test no.2
	 * Objective: To test name in range 2 to 30 characters
	 * Inputs: firstName = "Jack"
	 * Expected Output: True
	 */
	public void testValidateFirstName002() {
		try {
			boolean result = Customer.validateName("Jack");
			assertEquals(true, result);
		} catch (CustomerException e) {
			fail("Exception NOT expected");
		}
	}
	
	/*
	 * Test no.3
	 * Objective: To test name over 30 characters
	 * Inputs: firstName = "Jack"
	 * Expected Output: Exception message: "Name cannot be longer than 30 characters"
	 */
	public void testValidateFirstName003() {
		try {
			Customer.validateName("Jackfirstnamelongerthanthirtycharacters");
			fail("Exception expected");
		} catch (CustomerException e) {
			assertEquals("Name cannot be longer than 30 characters", e.getMessage());
		}
	}
	
	public void testValidateAddress001() {
		try {
			boolean result = Customer.validateAddress("N37 E6K2");
			assertEquals(true, result);
		} catch(CustomerException e) {
			fail("Exception not expected");
		}
	}
	
	public void testValidateAddress002() {
		try {
			Customer.validateAddress("N37e6K22");
			fail("Exception expected");
		} catch(CustomerException e) {
			assertEquals(e.getMessage(), "Not a valid Eircode");
		}
	}
	
	public void testPhoneNumber001() {
		try {
			Customer.validatePhoneNumber("89301748");
			fail("Exception expected");
		} catch (CustomerException e) {
			assertEquals(e.getMessage(), "Not a valid phone number");
		}
	}
	
	public void testPhoneNumber002() {
		try {
			boolean result = Customer.validatePhoneNumber("0852837645");
			assertEquals(true , result);
		} catch (CustomerException e) {
			fail("Exception not expected");
		}
	}
	
	public void testAddCustomer001() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
		Customer customer = new Customer();
		customer.setFirstName("Jane");
		customer.setLastName("Doe");
		customer.setPhoneNumber("0856667777");
		customer.setDeliveryArea("Coosan");
		customer.setAddress("N36 E5K6");
		try {
			boolean result = CustomerDao.addCustomer(db, customer);
			assertEquals(true, result);
		} catch (CustomerException e) {
			fail();
		}
	}
	
	public void testAddCustomer002() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
		Customer customer = new Customer();
		customer.setFirstName("Jane");
		customer.setLastName("Doe");
		customer.setPhoneNumber("0856667777r");
		customer.setDeliveryArea("Coosan");
		customer.setAddress("N36 E5K6");
		try {
			CustomerDao.addCustomer(db, customer);
			fail();
		} catch (CustomerException e) {
			assertEquals(e.getMessage(), "Not a valid phone number");
		}
	}
	
	public void testCustomerId001() {
		Customer customer = new Customer();
		String customerId = customer.getCustomerId();
		boolean result = customerId.matches("C[0-9]{6}");
		assertTrue(result);
	}
	
	public void testDeliveryArea001() {
		try {
			Customer.validateDeliveryArea("A");
			fail("Exception expected");
		} catch (CustomerException e) {
			assertEquals("Delivery area cannot be less than 5 characters", e.getMessage());
		}
	}
	
	public void testDeliveryArea002() {
		try {
			Customer.validateDeliveryArea("AVeryLongDeliveryAreaNameDefinitelyLongerThan35Characters");
			fail("Exception expected");
		} catch (CustomerException e) {
			assertEquals("Delivery area cannot be more than 35 characters", e.getMessage());
		}
	}
	
	public void testDeliveryArea003() {
		try {
			boolean res = Customer.validateDeliveryArea("Monksland");
			assertTrue(res);
		} catch (CustomerException e) {
			fail("Exception not expected");
		}
	}
}
