package tests;

import java.sql.SQLException;

import core.CustomerDao;
import db.DBUtil;
import exceptions.CustomerException;
import junit.framework.TestCase;

public class CustomerEditTest extends TestCase {
	
	private static DBUtil db;

	protected void setUp() throws Exception {
		db = DBUtil.getDBUtilInstance();
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public void testEditFirstName001() throws SQLException {
		try {
			CustomerDao.editCustomerFirstName(db, "C789090", "Jim");
		} catch (CustomerException e) {
			fail("Exception not expected");
		}
	}
	
	public void testEditFirstName002() throws SQLException {
		try {
			CustomerDao.editCustomerFirstName(db, "C789090", "B");
			fail("Exception expected");
		} catch (CustomerException e) {
		}
	}
	
	public void testEditLastName001() throws SQLException {
		try {
			CustomerDao.editCustomerLastName(db, "C789090", "Fallon");
		} catch (CustomerException e) {
			fail("Exception not expected");
		}
	}
	
	public void testEditLastName002() throws SQLException {
		try {
			CustomerDao.editCustomerLastName(db, "C789090", "B");
			fail("Exception expected");
		} catch (CustomerException e) {
		}
	}
	
	public void testEditPhoneNumber001() throws SQLException {
		try {
			CustomerDao.editCustomerPhoneNumber(db, "C789090", "0894752165");
		} catch (CustomerException e) {
			fail("Exception not expected");
		}
	}
	
	public void testEditPhoneNumber002() throws SQLException {
		try {
			CustomerDao.editCustomerPhoneNumber(db, "C789090", "04458644");
			fail("Exception expected");
		} catch (CustomerException e) {
		}
	}
	
	public void testEditDeliveryArea001() throws SQLException {
		try {
			CustomerDao.editCustomerDeliveryArea(db, "C789090", "Monksland");
		} catch (CustomerException e) {
			fail("Exception not expected");
		}
	}
	
	public void testEditDeliveryArea002() throws SQLException {
		try {
			CustomerDao.editCustomerDeliveryArea(db, "C789090", "B");
			fail("Exception expected");
		} catch (CustomerException e) {
		}
	}
	
	public void testEditEircode001() throws SQLException {
		try {
			CustomerDao.editCustomerEircode(db, "C789090", "N56 K75E");
		} catch (CustomerException e) {
			e.printStackTrace();
			fail("Exception not expected");
		}
	}
	
	public void testEditEircode002() throws SQLException {
		try {
			CustomerDao.editCustomerEircode(db, "C789090", "B");
			fail("Exception expected");
		} catch (CustomerException e) {
		}
	}
	
	/*
	 * Test no. 11
	 * Objective: To set a customer with no orders inactive
	 * Inputs: String "C819238"
	 * Expected Output: true
	 */
	public void testSetCustomerInactive001() throws SQLException {
		try {
			boolean success = CustomerDao.setInactive(db, "C819238");
			assertTrue(success);
		} catch (CustomerException e) {
			fail("Exception not expected");
		}
	}
	
	/*
	 * Test no. 12
	 * Objective: To set a customer with orders inactive
	 * Inputs: String "C123456"
	 * Expected Output: CustomerException("Customer has active orders")
	 */
	public void testSetCustomerInactive002() throws SQLException {
		try {
			CustomerDao.setInactive(db, "C123456");
			fail("Exception expected");
		} catch (CustomerException e) {
			assertEquals("Customer has active orders", e.getMessage());
		}
	}
	
	/*
	 * Test no. 13
	 * Objective: To set a customer with inactive orders inactive
	 * Inputs: String "C819223"
	 * Expected Output: CustomerException("Customer has active orders")
	 */
	public void testSetCustomerInactive003() throws SQLException {
		try {
			boolean success = CustomerDao.setInactive(db, "C819223");
			assertTrue(success);
		} catch (CustomerException e) {
			fail("Exception not expected");
		}
	}

}
