package tests;

import java.sql.ResultSet;
import java.sql.SQLException;

import core.Order;
import core.OrderDao;
import db.DBUtil;
import exceptions.IllegalOrderID;
import exceptions.IllegalOrderItemLength;
import exceptions.IllegalOrderPrice;
import exceptions.WrongCustomerIDFormatException;
import junit.framework.TestCase;

public class GenerateSummaryTesting extends TestCase {
	private static ResultSet resultSet;
	private static DBUtil dbUtil;

	protected void setUp() throws Exception {
		super.setUp();
		dbUtil = DBUtil.getDBUtilInstance();
		resultSet = null;
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		dbUtil.closeAll();
	}
	/***
	 * Test No.1
	 * Objective: To test delivered  order can be selected from the database.
	 * Inputs: Nothing
	 * Expected Output: There are some lists of order, if  some orders are delivered.
	 ***/
	public void testSearchDeliveredOrder() throws SQLException {
		resultSet = OrderDao.searchDeliveredOrder(dbUtil);
		resultSet.next();
		String contentString = resultSet.getString(1);
		assertNotNull(contentString); 
	}

	/***
	 * Test No.2
	 * Objective: To test undelivered  order can be selected from the database.
	 * Inputs: Nothing
	 * Expected Output: There are some lists of order, if  some orders are undelivered.
	 ***/
	public void testSearchUndeliveredOrder() throws SQLException {
		resultSet = OrderDao.searchUndeliveredOrder(dbUtil);
		resultSet.next();
		String contentString = resultSet.getString(1);
		assertNotNull(contentString);
	}

	/***
	 * Test No.3
	 * Objective: To test dispatched  order can be selected from the database.
	 * Inputs: Nothing
	 * Expected Output: There are some lists of order, if  some orders are dispatched. 
	 ***/
	public void testSearchDispatchedOrder() throws SQLException {
		resultSet = OrderDao.searchDispatchedOrder(dbUtil);
		resultSet.next();
		String contentString = resultSet.getString(1);
		assertNotNull(contentString);
	}

	/***
	 * Test No.4
	 * Objective: To test Unfulfilled  order can be selected from the database.
	 * Inputs: Nothing
	 * Expected Output: There are some lists of order, if  some orders are Unfulfilled.
	 ***/
	public void testSearchUndispatchedOrder() throws SQLException{
		resultSet = OrderDao.searchUnfulfilledOrder(dbUtil);
		resultSet.next();
		String contentString = resultSet.getString(1);
		assertNotNull(contentString);
	}

	/***
	 * Test No.5
	 * Objective: To test order summary can be printed out.
	 * Inputs: Nothing
	 * Expected Output: The order summary is printed  out  in the txt file, and no exception will be caught.
	 ***/
	public void testPrintOutOrderSummary() {
		try {
			OrderDao.printOutOrderSummary(dbUtil);
		} catch (Exception e) {
			fail();
		}
	}

}
