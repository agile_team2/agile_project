package tests;


import java.sql.SQLException;

import core.Order;
import core.OrderDao;
import db.DBUtil;
import exceptions.IllegalOrderID;
import exceptions.IllegalOrderItemLength;
import exceptions.IllegalOrderPrice;
import exceptions.WrongCustomerIDFormatException;
import junit.framework.TestCase;

/***
 * This class is testing for the generating order.
 * When the generateOrder method return true, it mean the order has been generated and synchronized in the database.
 * Otherwise, it is failed.
 * @author michael
 *
 */
public class GenerateOrderTesting extends TestCase{
	private static Order order;
    private static DBUtil dBUtil;
	
	protected void setUp() throws Exception {
		super.setUp();
		order = new Order();
		dBUtil = DBUtil.getDBUtilInstance();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		dBUtil.closeAll();
	}
	/***
	 * Test No.1
	 * Objective: To test the order can be inserted into database table.
	 * Inputs: customerID = "C123456", item = "The Independent", customerID exists in customer table.
	 * Expected Output: True
	 */
	public void testGenerateOrder001() throws SQLException, WrongCustomerIDFormatException, IllegalOrderID, IllegalOrderItemLength, IllegalOrderPrice{
		order.setCustomerId("C789090");
		order.setDescription("The Independent");
		order.setPrice(1.5);
		assertEquals(OrderDao.generateOrder(dBUtil, order),1);
	}
	
	/***
	 * Test No.2
	 * Objective: To test the order cannot be insert into database table.
	 * Inputs: customerID = "N123456789", item = "The Independent", customerID dose not exist in customer table.
	 * Expected Output: Throw a SQLException, and is caught.
	 */
	public void testGenerateOrder002() throws IllegalOrderPrice, WrongCustomerIDFormatException, IllegalOrderItemLength, SQLException, IllegalOrderID{
		order.setCustomerId("C123451");
		order.setDescription("The Independent");
		order.setPrice(1.5);
		try {
			OrderDao.generateOrder(dBUtil, order);
			fail();
		} catch (SQLException e) {
		}
		
	}

}