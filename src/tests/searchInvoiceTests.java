package tests;

import junit.framework.TestCase;

import java.awt.HeadlessException;
import java.sql.ResultSet;
import java.sql.SQLException;
import core.InvoiceDao;
import db.DBUtil;
import exceptions.IllegalOrderID;

public class searchInvoiceTests extends TestCase
{
	private static ResultSet resultSet;
	private static DBUtil dbUtil;
	protected void setUp() throws Exception {
		super.setUp();
		resultSet = null;
		dbUtil = DBUtil.getDBUtilInstance();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		dbUtil.closeAll();
	}
	
	/*
	 Test Number: 1
	 Test Type: JUnit
	 Test Objective: Test if i can search for an invoice by customer name
	 Inputs: firstName = Bill
	 Expected Output: a not null ResultSet object
	 */
	
	public void testsearchInvoiceByCustomerFirstName001() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
		String content = null;
		resultSet = InvoiceDao.searchInvoiceByCustomerFirstName("Bill", dbUtil);
		resultSet.next();
		content = resultSet.getString(1);
		assertNotNull(content);
	}
		
	/*
	 Test Number: 2
	 Test Type: JUnit
	 Test Objective: Test if i can search for an invoice by customer name
	 Inputs: lastName = Gates
	 Expected Output: a not null ResultSet object
	 */
	
	public void testsearchInvoiceByCustomerLastName001() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
		String content = null;
		resultSet = InvoiceDao.searchInvoiceByCustomerLastName("Gates", dbUtil);
		resultSet.next();
		content = resultSet.getString(1);
		assertNotNull(content);
	}
	
	/*
	 Test Number: 3
	 Test Type: JUnit
	 Test Objective: Test if i can search for an invoice by order id
	 Inputs: 123
	 Expected Outputs: a not null ResultSet object
	 */
	 
	public void testsearchInvoiceByOrderID001() throws HeadlessException, IllegalOrderID, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
		String content = null;
		resultSet = InvoiceDao.searchInvoiceByOrderID("O176639", dbUtil);
		resultSet.next();
		content = resultSet.getString(1);
		assertNotNull(content);
	}
	/*
	 Test Number: 4
	 Test Type: JUnit
	 Test Objective: Test if i can be search for an invoice by date
	 Inputs: 19/02/28
	 Expected Outputs: a not null ResultSet object
	 */
	
	public void testsearchInvoiceByDate001() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
		String dateString = "2019-03-16";
		assertNotNull(InvoiceDao.searchInvoiceByDate(dateString,dbUtil));
	}
	
	/*
	 Test Number: 5
	 Test Type: JUnit
	 Test Objective: Test if i can be search for an invoice by month
	 inputs: 2
	 Expected Outputs: a null String object
	 */
	
	public void testsearchInvoiceByMonth001() throws SQLException
	{
		String monthString = "3";
		resultSet = InvoiceDao.searchInvoiceByMonth(monthString, dbUtil);
		String content = null;
		resultSet.next();
		content = resultSet.getString(1);
		assertNotNull(content);
	}
}
