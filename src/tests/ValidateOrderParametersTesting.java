package tests;



import core.Order;
import exceptions.IllegalOrderID;
import exceptions.IllegalOrderItemLength;
import exceptions.IllegalOrderPrice;
import exceptions.WrongCustomerIDFormatException;
import junit.framework.TestCase;

public class ValidateOrderParametersTesting extends TestCase {
	
	public static String getLengthEqual257String() {
		StringBuilder builder = new StringBuilder();
		for(int i = 1;i <= 257;i ++) {
			builder.append("a");
		}
		return new String(builder);
	}
	
	public static String getLengthEqual256String() {
		StringBuilder builder = new StringBuilder();
		for(int i = 1;i <= 256;i ++) {
			builder.append("a");
		}
		return new String(builder);
	}

	/***
	 * Test No.1
	 * Objective: To test right format customerID
	 * Inputs: customerID = "N123456789"
	 * Expected Output: True
	 */

	public void testCustomerID001() throws Exception {
		assertTrue(Order.validateCustomerId("C123456"));
	}

	/***
	 * Test No.2
	 * Objective: To test wrong format customerId
	 * Inputs: customerID = "S123456789"
	 * Expected Output: Exception Msg: "Wrong customerID format"
	 */

	public void testCustomerID002() throws Exception{
		try {
			Order.validateCustomerId("S123456");
			fail();
		}catch (WrongCustomerIDFormatException e) {
			assertEquals("The customerID's fomat is wrong.", e.getMessage());
		}
	}

	/***
	 * Test No.3
	 * Objective: Item description's length is over 256
	 * Inputs:description's length is over 256
	 * Expected Output: Exception Msg: "The order item's length is illegal."
	 */

	public void testDescription001() throws Exception{
		try {
			Order.validateDescription(getLengthEqual257String());
			fail();
		}catch (IllegalOrderItemLength e) {
			assertEquals("The order item's length is illegal.", e.getMessage());
		}
	}

	/***
	 * Test No.4
	 * Objective: Item description's length is 0
	 * Inputs:description =  ""
	 * Expected Output: Exception Msg: "The order item's length is illegal."
	 */

	public void testDescription002() throws Exception{
		try {
			Order.validateDescription("");
			fail();
		}catch (IllegalOrderItemLength e) {
			assertEquals("The order item's length is illegal.", e.getMessage());
		}
	}

	/***
	 * Test No.5
	 * Objective: Item description's length is 11
	 * Inputs:description  =  "Independent"
	 * Expected Output: validateDescription() will return true.
	 */

	public void testDescription003() throws Exception{
		assertTrue(Order.validateDescription("Independent"));
	}

	/***
	 * Test No.6
	 * Objective: Price is below 0
	 * Inputs:price = 0
	 * Expected Output: Exception Msg: "The order price is illegal!"
	 */

	public void testPrice001(){
		try {
			Order.validatePrice(0);
			fail();
		} catch (IllegalOrderPrice e) {
			assertEquals("The order price is illegal!", e.getMessage());
		}

	}

	/***
	 * Test No.7
	 * Objective: Price is 1000
	 * Inputs:price = 1000
	 * Expected Output: validatePrice() will return true.
	 */

	public void testPrice002() throws IllegalOrderPrice{
		assertTrue(Order.validatePrice(1000));
	}
	
	/***
	 * Test No.8
	 * Objective: Price is over 1000
	 * Inputs:price = 1001
	 * Expected Output: Exception Msg: "The order price is illegal!"
	 */

	public void testPrice003(){
		try {
			Order.validatePrice(1001);
			fail();
		} catch (IllegalOrderPrice e) {
			assertEquals("The order price is illegal!", e.getMessage());
		}

	}
	
	/***
	 * Test No.9
	 * Objective: Test if the orderId is valid.
	 * Inputs:orderId = "N123456"
	 * Expected Output: validateOrderID() will return true.
	 */

	public void testOrderId001() throws IllegalOrderID{
		assertTrue(Order.validateOrderID("O123456"));

	}
	
	/***
	 * Test No.10
	 * Objective: Test if the orderId is valid.
	 * Inputs:orderId = "15"
	 * Expected Output:  Exception Msg: "This order ID is invalid!"
	 */

	public void testOrderId002(){
		try {
			assertTrue(Order.validateOrderID("N1"));
			fail();
		} catch (IllegalOrderID e) {
			assertEquals(e.getMessage(), "This order ID is invalid!");
		}

	}
	
	/***
	 * Test No.11
	 * Objective: Item description's length is 256
	 * Inputs:description which length is 256
	 * Expected Output: validateDescription() will return true.
	 */

	public void testDescription004() throws Exception{
		assertTrue(Order.validateDescription(getLengthEqual256String()));
	}
	
	/***
	 * Test No.12
	 * Objective: Price is below 0 or equal 0
	 * Inputs:price = 0
	 * Expected Output: Exception Msg: "The order price is illegal!"
	 */

	public void testPrice004(){
		try {
			Order.validatePrice(0);
			fail();
		} catch (IllegalOrderPrice e) {
			assertEquals("The order price is illegal!", e.getMessage());
		}

	}

}
