package exceptions;

public class CustomerException extends Exception {
	
	private String message;
	
	public CustomerException(String errorMessage) {
		this.message = errorMessage;
	}
	
	public String getMessage() {
		return this.message;
	}
}
