package exceptions;

public class InvalidPaymentMethodException extends Exception
{
	public InvalidPaymentMethodException() {
	super("This invoice payment method is invalid.");
	}
}
