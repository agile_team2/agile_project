package exceptions;

public class InvalidInvoiceIDException extends Exception
{
	public InvalidInvoiceIDException() {
		super("This invoice ID is invalid.");
	}
}
