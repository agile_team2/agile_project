package exceptions;

public class IllegalOrderID extends Exception {
	public IllegalOrderID() {
		super("This order ID is invalid!");
	}

}
