package exceptions;

public class IllegalOrderItemLength extends Exception{
	public IllegalOrderItemLength() {
		super("The order item's length is illegal.");
	}
}
