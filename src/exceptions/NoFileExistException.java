package exceptions;

public class NoFileExistException extends Exception {
       public NoFileExistException() {
    	   super("No File Found");
       }
}
