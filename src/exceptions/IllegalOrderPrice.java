package exceptions;

public class IllegalOrderPrice extends Exception {
	public IllegalOrderPrice() {
		super("The order price is illegal!");
	}

}
