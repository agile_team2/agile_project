DROP DATABASE IF EXISTS Newsagency_DB;
CREATE DATABASE Newsagency_DB;
USE Newsagency_DB;

DROP TABLE IF EXISTS Customers;
/* Customer Table */
CREATE TABLE Customers (
	customer_id char(7) not null PRIMARY KEY,
	firstName VARCHAR(35) NOT NULL,
    lastName VARCHAR(35) NOT NULL,
	phone_no VARCHAR(35) NOT NULL,
    deliveryArea varchar(35) not null,
	address VARCHAR(35) NOT NULL);
    
drop table if exists InactiveCustomers;
/* Inactive custoemr table */
Create table InactiveCustomers (
	customer_id char(7) not null PRIMARY KEY,
	firstName VARCHAR(35) NOT NULL,
    lastName VARCHAR(35) NOT NULL,
	phone_no VARCHAR(35) NOT NULL,
    deliveryArea varchar(35) not null,
	address VARCHAR(35) NOT NULL);

DROP TABLE IF EXISTS Orders;
/* Orders Table */
CREATE TABLE Orders(
	order_id char(7) Not NULL PRIMARY KEY,
    customer_id char(7) not null,
    order_date date,
    order_description VARCHAR(35) NOT NULL,
    order_price decimal(5,2) NOT NULL,
    isDispatched varchar(3) default null,
    isDelivered varchar(3) default null,
    isActive varchar(3) default "yes",
    FOREIGN KEY (customer_id) REFERENCES  Customers (customer_id));

DROP TABLE IF EXISTS Users;
/* Users Table */
CREATE TABLE Users(
	user_id INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    username VARCHAR(50) NOT NULL,
    user_password VARCHAR(50) NOT NULL,
    user_job VARCHAR(35) NOT NULL);

DROP TABLE IF EXISTS Invoices;
/* Invoices Table */
CREATE TABLE Invoices(
	invoice_id char(7)NOT NULL PRIMARY KEY,
    invoice_date date,
    payment_method VARCHAR(50) NOT NULL,
    isOverdue VARCHAR(3),
    isPaidFor VARCHAR(3),
    order_id char(7) Not Null,
    FOREIGN KEY (order_id) REFERENCES Orders (order_id));


-- Mock Customers
Insert Into Customers Values
("C123456", "John", "Doe", "0879464637", "Glasson", "N67 T4K7"),
("C123455", "Bill", "Gates", "0865643727", "Willow Park", "N37 E4K2"),
("C789090", "Jake", "Farrelly", "0862224455", "Coosan", "N24 E7K8"),
("C123457", "Tim", "Sharp", "0896655223", "Willow Park", "N37 E6K3"), 
("C123458", "Jim", "Sharp", "0835479852", "Irish Town", "N37 E6K3"),
("C864859", "Tom", "Donoghue", "0875674352", "Summerhill", "N32 Y6K3"),
("C864495", "Sinead", "Byrne", "0837589823", "Monksland", "N37 E4K6"),
("C819238", "David", "Fallon", "0858765674", "Coosan", "N37 E5K2"),
("C819223", "Jack", "Sheehan", "0838749987", "Glasson", "N43 ET7K");

-- Mock Inactive Customers
Insert into InactiveCustomers values
("C847129", "Ciara", "Mahon", "0896748272", "Irish Town", "N37 E5K4");

-- Mock Orders
INSERT INTO Orders VALUES 
("O129580", "C123456", "2019-03-16", "Weekly newspapers", "1.00", "Yes", "Yes","Yes"), 
("O176639", "C123455", "2019-03-16", "The Independent", "1.50", "Yes", "Yes","Yes"), 
("O348755", "C123457", "2019-03-16", "The Independent", "1.50", "Yes", null,"Yes"), 
("O364078", "C123458", "2019-03-16", "The Independent", "1.50", "Yes", null,"Yes"), 
("O387605", "C789090", "2019-03-16", "The Independent", "1.50", null, null,"Yes"),
("O837272", "C864859", "2019-04-02", "The Irish Times", "1.50", null, null, null),
("O129305", "C819223", "2019-04-02", "The Irish Times", "1.50", null, null, null);

-- Mock Invoices
Insert Into Invoices Values("I123459", "2019-03-16", "cash", null, "Yes", "O129580"),
("I123490", "2019-03-16", "cheque", null, "Yes", "O176639"),
("I167845", "2019-03-16", "cash", "Yes", null, "O348755"),
("I874327", "2019-03-16", "cheque", null, "Yes", "O364078"),
("I623674", "2019-03-16", "cash", "Yes", null, "O387605");

insert into users values(null, "admin", "admin", "admin");
